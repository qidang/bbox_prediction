#import _init
import sys
import os
import argparse
import platform
from torchcore.dnn.networks.detection_heads.retina_head import RetinaNetHead
import numpy as np
import datetime

from torchvision.models.detection.retinanet import RetinaNet as RetinaNetTV
from torchcore.dnn.networks.retinanet import RetinaNet
from torchcore.dnn.networks.retinanet_roi import RetinaRoINet
from torchcore.tools import Logger
import torchvision
import math
from torch import nn
import json
import tqdm
import time
from pprint import pprint

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
import datetime
from torchcore.data.transforms import RandomMirror, RandomAbsoluteScale, RandomCrop, Compose, AddSurrandingBox, AddPersonBox

from PIL.ImageDraw import Draw

from rcnn_config import config
#from tools import torch_tools
#from data import data_feeder

from torchcore.data.datasets import ModanetDataset, ModanetHumanDataset, COCOPersonDataset, COCODataset, COCOTorchVisionDataset
from torchcore.data.datasets.fashion_pedia import FashionPediaDataset
#from rcnn_dnn.networks import networks
#from rcnn_dnn.data.collate_fn import collate_fn, CollateFnRCNN
from torchcore.data.collate import CollateFnRCNN, collate_fn_torchvision

import torch
#torch.multiprocessing.set_sharing_strategy('file_system')
import torchvision.transforms as transforms
import torch.optim as optim

from torchcore.dnn import trainer,DistributedTrainer
from torchcore.dnn.networks.faster_rcnn_fpn import FasterRCNNFPN
from torchcore.dnn.networks.faster_rcnn_roi_fpn import FasterRCNNRoIFPN
from torchcore.dnn.networks.roi_net import RoINet
from torchcore.dnn.networks.rpn import MyAnchorGenerator, MyRegionProposalNetwork, RoIAnchorGenerator
from torchcore.dnn.networks.heads import RPNHead, RetinaHead, retina_head
from torchcore.dnn import networks
from torchcore.dnn.networks.tools.data_parallel import DataParallel
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.utils.data.distributed import DistributedSampler

from torchcore.engine.launch import launch
import torch.distributed as dist
from torchcore.evaluation import COCOEvaluator

from torchcore.dnn.networks.feature.resnet import roi_resnet50
from torchcore.dnn.networks.necks import FeaturePyramidNetwork, LastLevelMaxPool, LastLevelP6P7

def parse_commandline():
    parser = argparse.ArgumentParser(description="Training the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    parser.add_argument('-b','--batch_size',help='Batch size per step per gpu', required=True, type=int)
    parser.add_argument('-a','--accumulation_step',help='Accumulate size', required=False, default=1, type=int)
    parser.add_argument('--gpu_num',help='gpu used to train', required=False, default=1, type=int)
    parser.add_argument('--machine_num',help='machine used to train', required=False, default=1, type=int)
    parser.add_argument('-t','--tag',help='Model tag', required=False)
    parser.add_argument('--resume',help='resume the model', action='store_true', required=False)
    parser.add_argument('--load_model_path',help='load weights for the model', default=None, required=False)
    parser.add_argument('--evaluate',help='Do you just want to evaluate the model', action='store_true', required=False)
    parser.add_argument('--freeze_bn',help='Do you just want to freeze the bn statistcs during the training, the bn layer is still trainable in this mode', action='store_true', required=False)
    parser.add_argument('--dataset', help='The dataset we are going to use', default='coco_person')
    parser.add_argument('--lr',help='initial learning rate', required=False, type=float, default=None)
    parser.add_argument('--torchvision_model', help='Do we want to use torchvision model', action='store_true')
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return parser.parse_args()


def get_absolute_box(human_box, box):
    #box[2]+=int(human_box[0])+box[0]
    #box[3]+=int(human_box[1])+box[1]
    box[0]+=int(human_box[0])
    box[1]+=int(human_box[1])
    return box

class my_trainer(DistributedTrainer):

    def validate_onece(self):
        self.validate()

    def validate(self):
        if isinstance(self._model, DDP):
            if not self.is_main_process():
                print('one process resturn')
                return
        if self._cfg.torchvision_model:
            self._validate_torchvision()
        else:
            self._validate()

    def _validate( self ):
        print('start to validate')
        self._model.eval()
        if isinstance(self._model, DDP):
            test_model = self._model.module
        else:
            test_model = self._model

        total_time = 0

        results = []
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(tqdm.tqdm(self._testset, 'evaluating')):
                inputs = self._set_device( inputs )
                targets = self._set_device( targets )
                start =time.time()
                output = test_model( inputs, targets)
                batch_size = len(output['boxes'])
                #for i, im in enumerate(output):
                for i in range(batch_size):
                    if len(output['boxes'][i]) == 0:
                        continue
                    # convert to xywh
                    output['boxes'][i][:,2] -= output['boxes'][i][:,0]
                    output['boxes'][i][:,3] -= output['boxes'][i][:,1]
                    for j in range(len(output['boxes'][i])):
                        results.append({'image_id':int(targets[i]['image_id']), 
                                        'category_id':output['labels'][i][j].cpu().numpy().round(decimals=2).tolist(), 
                                        'bbox':output['boxes'][i][j].cpu().numpy().round(decimals=2).tolist(), 
                                        'score':output['scores'][i][j].cpu().numpy().round(decimals=2).tolist()})
                total_time += time.time()-start
        print('total time is {}s'.format(total_time))
        if isinstance(self._model, DDP):
            model_time = self._model.module.total_time
        else:
            model_time = self._model.total_time
        print('total model time is {}s'.format(model_time))
        print('total model time is {}s'.format(sum(model_time.values())))
        print('average time per image is {} s'.format(sum(model_time.values())/len(self._testset)))
        average_model_time = {k:v/len(self._testset) for k,v in model_time.items()}
        print('model average time per image by part is {}'.format(average_model_time))
        result_path = '{}temp_result.json'.format(self._tag)
        with open(result_path,'w') as f:
            json.dump(results,f)
        self.evaluator.evaluate(result_path)

    def _validate_torchvision( self ):
        print('start to validate')
        self._model.eval()
        if isinstance(self._model, DDP):
            test_model = self._model.module
        else:
            test_model = self._model

        results = []
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(tqdm.tqdm(self._testset, 'evaluating')):
                inputs = self._set_device( inputs )
                #if self._cfg.torchvision_model:
                #    output = self._model( inputs['data'])
                #else:
                output = test_model( inputs)
                batch_size = len(output)
                #for i, im in enumerate(output):
                for i in range(batch_size):
                    if len(output[i]['boxes']) == 0:
                        continue
                    # convert to xywh
                    output[i]['boxes'][:,2] -= output[i]['boxes'][:,0]
                    output[i]['boxes'][:,3] -= output[i]['boxes'][:,1]
                    for j in range(len(output[i]['boxes'])):
                        results.append({'image_id':int(targets[i]['image_id']), 
                                        'category_id':output[i]['labels'][j].cpu().numpy().round(decimals=2).tolist(), 
                                        'bbox':output[i]['boxes'][j].cpu().numpy().round(decimals=2).tolist(), 
                                        'score':output[i]['scores'][j].cpu().numpy().round(decimals=2).tolist()})
                #output = self._model['net']( inputs, just_embedding=True) # debug
                #bench.update( targets, output )
                
        out_path = '{}_result.json'.format(self._tag)
        with open(out_path,'w') as f:
            json.dump(results,f)
        self.evaluator.evaluate(out_path)
        #self.eval_result(out_path, anno_type='bbox', dataset=self._dataset_name)
        #self.eval_result(out_path, anno_type='segm', dataset=self._dataset_name)

    def eval_result(self, dt_json, anno_type, dataset='coco_person'):
        #annType = ['segm','bbox','keypoints']
        if dataset not in ['coco','coco_person', 'modanet', 'fashionpedia']:
            raise ValueError('only support coco, coco_person and modanet dataset')
        if dataset == 'coco_person':
            gt_json=os.path.expanduser('~/data/datasets/COCO/annotations/instances_val2014.json')
        elif dataset == 'coco':
            gt_json=os.path.expanduser('~/data/datasets/COCO/annotations/instances_val2017.json')
        elif dataset =='modanet':
            gt_json=os.path.expanduser('~/data/datasets/modanet/Annots/modanet_instances_val.json')
        elif dataset =='fashionpedia':
            gt_json=os.path.expanduser('~/data/datasets/Fashionpedia/annotations/instances_attributes_val2020.json')
        #dt_json='temp_result.json'

        # we need to map the category ids back
        if dataset == 'coco':
            print('revise coco dataset label to gt')
            cat_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 67, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 84, 85, 86, 87, 88, 89, 90]
            with open(dt_json) as f:
                results = json.load(f)
                for result in results:
                    temp_id = result['category_id']
                    result['category_id'] = cat_ids[temp_id-1]
            #with open(dt_json,'w') as f:
            #    json.dump(results,f)
        elif dataset == 'fashionpedia':
            print('revise fashionpedia dataset label to gt')
            with open(dt_json) as f:
                results = json.load(f)
                for result in results:
                    temp_id = result['category_id']
                    result['category_id'] = temp_id-1
            #with open(dt_json,'w') as f:
            #    json.dump(results,f)
        else:
            results = dt_json
        annType = anno_type
        cocoGt=COCO(gt_json)
        cocoDt=cocoGt.loadRes(results)

        imgIds=sorted(cocoGt.getImgIds())

        # running evaluation
        cocoEval = COCOeval(cocoGt,cocoDt,annType)
        if dataset == 'coco_person':
            cocoEval.params.catIds = [1]
        cocoEval.params.imgIds = imgIds
        cocoEval.evaluate()
        cocoEval.accumulate()
        cocoEval.summarize()

    def _set_scheduler(self):
        scheduler = self._cfg.scheduler
        if scheduler.type == 'multi_step':
            self._scheduler = optim.lr_scheduler.MultiStepLR( self._optimizer,
                                                            milestones=scheduler['milestones'],
                                                            gamma=scheduler.get('gamma', 0.1))
        else:
            raise ValueError('unknow scheduler {}'.format(scheduler.type))

    def init_logger(self):
        train_path = self._cfg.path['LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=train_path, console=False, console_formatter=console_formatter)

        print('Loss log path is: {}'.format(train_path))
    

def load_checkpoint(model, path, device, to_print=True):
    #checkpoint = torch.load(path)
    state_dict_ = torch.load(path, map_location=device)['model_state_dict']
    state_dict = {}
    for k in state_dict_:
        if k.startswith('module') and not k.startswith('module_list'):
            state_dict[k[7:]] = state_dict_[k]
        else:
            state_dict[k] = state_dict_[k]
    model.load_state_dict(state_dict, strict=True )
    #self._epoch = checkpoint['epoch']
    #self._model.load_state_dict(checkpoint['model_state_dict'])
    #self._optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    if to_print:
        print('Chekpoint has been loaded from {}'.format(path))

def get_model(cfg, torchvision_model=False):
    if torchvision_model:
        print('use torchvision model')
        #backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet50', pretrained=True)
        #model = torchvision.models.detection.FasterRCNN(backbone, num_classes=cfg.class_num, min_size=cfg.min_size, max_size=cfg.max_size)
        #model = torchvision.models.detection.MaskRCNN(backbone, num_classes=cfg.class_num, min_size=cfg.min_size, max_size=cfg.max_size)
        backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet50', pretrained=True, returned_layers=[2, 3, 4], extra_blocks=torchvision.ops.feature_pyramid_network.LastLevelP6P7(256, 256))
        model = torchvision.models.detection.RetinaNet(backbone, num_classes=cfg.class_num, min_size=cfg.min_size, max_size=cfg.max_size)
    else:
        roi_layer = 1
        backbone = roi_resnet50(pretrained=True, roi_w=cfg.roi_w, roi_h=cfg.roi_h, roi_layer=roi_layer, returned_layers=[2,3,4])
        neck = FeaturePyramidNetwork([512, 1024,2048], 256, LastLevelP6P7(256,256))
        heads = {}
        #base_stride = 8 # the base stride should count start from the first input feature map of the retina head
        base_stride = tuple((8*2**i, 8*2**i) for i in range(5))
        anchor_sizes = tuple((x, int(x * 2 ** (1.0 / 3)), int(x * 2 ** (2.0 / 3))) for x in [32, 64, 128, 256, 512])
        anchor_generator = RoIAnchorGenerator(base_stride, sizes=anchor_sizes, aspect_ratios=(0.5,1.0,2.0))
        #anchor_generator = MyAnchorGenerator(sizes=((32,),(64,),(128,),(256,),(512,)), aspect_ratios=(0.5,1.0,2.0))
        #anchor_generator = MyAnchorGenerator(sizes=((32,),(64,),(128,),(256,)), aspect_ratios=(0.5,1.0,2.0))
        num_anchors_per_location = anchor_generator.num_anchors_per_location()
        assert all(num_anchors_per_location[0] == item for item in num_anchors_per_location)
        retina_head = RetinaHead(in_channels=256, num_anchors=num_anchors_per_location[0], num_classes=cfg.class_num)
        heads['retina_head'] = RetinaNetHead(retina_head, anchor_generator, cfg.retinahead)
        model = RetinaRoINet(backbone=backbone, target_converter_cfg=cfg.target_converter, neck=neck, heads=heads, roi_box_key='input_box', debug_time=True)
    return model


def run(args) :
    world_size = args.gpu_num * args.machine_num
    world_batch_size = world_size * args.batch_size
    if args.gpu_num == 1:
        rank = 0
    else:
        rank = dist.get_rank()

    params = {}
    params['config'] = 'retinanet_roi_{}'.format(args.dataset)
    params['tag'] = args.tag
    params['path'] = {}
    batch_size_per_gpu_per_accumulation = args.batch_size

    params['path']['project'] = os.path.expanduser('~/Vision/data')

    cfg = config( params['config'], params['path']['project'] )
    cfg.update( args )
    cfg.update_lr(world_batch_size)

    original_transform=False
    if original_transform:
        min_size = 800
        max_size = 1333
    else:
        min_size = 1024
        max_size = 1024
    cfg.min_size = min_size
    cfg.max_size = max_size
    cfg.torchvision_model = args.torchvision_model

    if args.lr is not None:
        cfg.optimizer.lr = args.lr

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], args.dataset, model_hash='frcnn' )
    if rank == 0:
        print(cfg)
    

    #t_person_box = AddPersonBox('fashionpedia_train_person.pkl', add_extra_dim=True, random_scale_and_crop=[0.5,2])
    t_person_box = AddPersonBox('fashionpedia_train_person.pkl', add_extra_dim=True, random_scale_and_crop=[0.8,1.3])
    t_mirrow = RandomMirror(probability=0.5, targets_box_keys=['boxes', 'input_box'], mask_key=None)
    #t_mirrow = RandomMirror(probability=0.5, targets_box_keys=['boxes'], mask_key=None)
    #t_scale = RandomAbsoluteScale(cfg.max_size/2, cfg.max_size*2, targets_box_keys=['boxes', 'input_box'], mask_key=None)
    #t_scale = RandomAbsoluteScale(cfg.max_size/2, cfg.max_size*2, targets_box_keys=['boxes'], mask_key=None)
    t_scale = RandomAbsoluteScale(cfg.max_size, cfg.max_size, targets_box_keys=['boxes', 'input_box'], mask_key=None)
    #t_crop = RandomCrop(cfg.max_size, box_inside=True, mask_key='masks')
    t_crop = RandomCrop(cfg.max_size, box_inside=True, mask_key=None)
    t_surround_box = AddSurrandingBox(box_name='target_box')
    #transforms = Compose([t_person_box, t_mirrow, t_scale, t_surround_box])
    if original_transform:
        transforms = Compose([t_mirrow])
    else:
        transforms = Compose([t_person_box, t_mirrow, t_scale])
        #transforms = Compose([t_mirrow, t_scale, t_crop])

    #t_person_box = AddPersonBox('fashionpedia_val_person.pkl', add_extra_dim=True, extend_to_target_boxes=True,extra_padding=4)
    t_person_box = AddPersonBox('fashionpedia_val_person.pkl', add_extra_dim=True, extend_to_target_boxes=True)
    #t_person_box = AddPersonBox('fashionpedia_val_person.pkl', add_extra_dim=True )
    t_scale = RandomAbsoluteScale(cfg.max_size, cfg.max_size, targets_box_keys=['boxes', 'input_box'], mask_key=None)
    test_transforms = Compose([t_person_box, t_scale])
    #test_transforms = None

    #transforms = Compose([t_mirrow, t_crop])
    #transforms = None
    xyxy = True

    if args.dataset == 'coco_person':
        root = os.path.expanduser('~/data/datasets/COCO')
        anno = os.path.expanduser('~/data/annotations/coco2014_instances_person_all.pkl')
        dataset = COCOPersonDataset(root, anno, part='train2014', transforms=transforms)
        test_dataset = COCOPersonDataset(root, anno, part='val2014', transforms=None)
        cfg.class_num = 1
    elif args.dataset == 'modanet':
        anno_path = os.path.expanduser('~/data/annotations/modanet2018_instances_revised.pkl')
        root = os.path.expanduser('~/data/datasets/modanet/Images')
        dataset = ModanetDataset(anno=anno_path, part='train', root=root, transforms=transforms) # the default transform in faster rcnn will be used
        test_dataset = ModanetDataset(anno=anno_path, part='val', root=root, transforms=None) # the default transform in faster rcnn will be used
        cfg.class_num = 13
    elif args.dataset == 'coco':
        anno_path = os.path.expanduser('~/data/annotations/coco2017_instances.pkl')
        root = os.path.expanduser('~/data/datasets/COCO')
        if args.torchvision_model:
            dataset = COCOTorchVisionDataset(anno=anno_path, part='train2017', root=root, transforms=transforms ) # the default transform in faster rcnn will be used
            test_dataset = COCOTorchVisionDataset(anno=anno_path, part='val2017', root=root, transforms=None ) # the default transform in faster rcnn will be used
            cfg.class_num = 81
        else:
            dataset = COCODataset(anno=anno_path, part='train2017', root=root, transforms=transforms, xyxy=xyxy) # the default transform in faster rcnn will be used
            test_dataset = COCODataset(anno=anno_path, part='val2017', root=root, transforms=None, xyxy=xyxy) # the default transform in faster rcnn will be used
            cfg.class_num = 80
    elif args.dataset == 'fashionpedia':
        anno_path_train = os.path.expanduser('~/data/annotations/fashionpedia_instances_train.pkl')
        anno_path_val = os.path.expanduser('~/data/annotations/fashionpedia_instances_val.pkl')
        root = os.path.expanduser('~/data/datasets/Fashionpedia')
        if args.torchvision_model:
            dataset = FashionPediaDataset(anno=anno_path_train, part='train', root=root, transforms=transforms, add_mask=True, torchvision_format=True) # the default transform in faster rcnn will be used
            test_dataset = FashionPediaDataset(anno=anno_path_val, part='val', root=root, transforms=None, add_mask=True, torchvision_format=True) # the default transform in faster rcnn will be used
            cfg.class_num = 47
        else:
            dataset = FashionPediaDataset(anno=anno_path_train, part='train', root=root, transforms=transforms, add_mask=False) # the default transform in faster rcnn will be used
            test_dataset = FashionPediaDataset(anno=anno_path_val, part='val', root=root, transforms=test_transforms, add_mask=False) # the default transform in faster rcnn will be used
            cfg.class_num = 46

    cfg.retinahead.class_num = cfg.class_num

    if args.torchvision_model:
        collate_fn_rcnn_train = collate_fn_torchvision
        collate_fn_rcnn_test = collate_fn_torchvision
    else:
        if original_transform:
            resized = False
        else:
            resized = True
        collate_fn_rcnn_train = CollateFnRCNN(min_size=cfg.min_size, max_size=cfg.max_size, resized=resized) # set resized to True if the image is cropped before
        collate_fn_rcnn_test = CollateFnRCNN(min_size=cfg.min_size, max_size=cfg.max_size)

    #collate_fn_rcnn = CollateFnRCNN(min_size=416, max_size=416)

    dist_train_sampler = None
    if world_size>1:
        # NOTE: This is a Sampler, NOT a batch sampler
        dist_train_sampler = DistributedSampler(dataset)
        data_loader = torch.utils.data.DataLoader(
                dataset, 
                #batch_size=batch_size_per_gpu_per_accumulation, 
                shuffle=(dist_train_sampler is None), 
                num_workers=2,
                #batch_sampler=dist_train_sampler,
                sampler=dist_train_sampler,
                batch_size=batch_size_per_gpu_per_accumulation,
                pin_memory=True,
                #drop_last=False,
                collate_fn=collate_fn_rcnn_train)
    else:
        data_loader = torch.utils.data.DataLoader(
                dataset, 
                batch_size=batch_size_per_gpu_per_accumulation, 
                shuffle=True, 
                num_workers=4,
                pin_memory=True,
                drop_last=True,
                collate_fn=collate_fn_rcnn_train)

    test_data_loader = torch.utils.data.DataLoader(
      test_dataset, 
      batch_size=1, 
      shuffle=False,
      num_workers=0,
      pin_memory=True,
      drop_last=False,
      collate_fn=collate_fn_rcnn_test
    )

    #data_loader_test = torch.utils.data.DataLoader(
    #    val_dataset, batch_size=1, shuffle=False, num_workers=4,
    #    collate_fn=collate_fn_rcnn)

    device = torch.device( rank )

    model = get_model(cfg, torchvision_model=args.torchvision_model)
    model = model.to(rank)


    if world_size > 1:
        ddp_model = DDP(model, device_ids=[rank])
    else:
        ddp_model = model

    evaluator = COCOEvaluator(dataset_name=args.dataset, evaluate_type=['bbox'])

    t = my_trainer( cfg, ddp_model, device, data_loader, testset=test_data_loader, dataset_name=args.dataset, train_sampler=dist_train_sampler, benchmark=None, tag=args.tag,evaluator=evaluator, epoch_based=False, eval_step_interval=10000, save_step_interval=10000, rank=rank, log_print_iter=1000 )
    if not args.evaluate:
        t.train()
    else:
        if args.load_model_path is not None:
            load_checkpoint(model, args.load_model_path, device, to_print=True)
        t.validate()

def cleanup():
    dist.destroy_process_group()

def main(args):
    launch(run, num_gpus_per_machine=args.gpu_num, args=(args,), dist_url='auto')

if __name__=="__main__" :
    args = parse_commandline()
    main(args)
