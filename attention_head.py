import torch
from torch.nn.parameter import Parameter
from torch.nn import Transformer
from torch import nn

class AttentionHead(torch.nn.Module):
    def __init__(self, cfg) -> None:
        super().__init__()
        self.cfg = cfg
        #self.source_linear = nn.Linear(in_features=cfg.source_dim, out_features=cfg.feature_dim)
        self.target_linear = nn.Linear(in_features=cfg.target_dim, out_features=cfg.feature_dim)
        self.transfromer = Transformer(d_model=cfg.feature_dim, nhead=4, num_decoder_layers=1, num_encoder_layers=1, dim_feedforward=cfg.out_feature_num)

    def forward(self, source:torch.Tensor, target: torch.Tensor ):
        '''
            source: input for transformer encoder: NCHW -> size (S, N, E)
            target: input for transformer deconder: NCHW -> size (T, N, E), T should be 1
        '''
        #N, C, H, W = source.shape
        #Nt, Ct, Ht, Wt = target.shape

        source = source.flatten(2).permute(2,0,1)

        target = target.flatten(start_dim=1)
        target = self.target_linear(target)
        target = target.unsqueeze(0)
        
        out = self.transfromer(source, target).squeeze(0)
        return out
