#import _init
import os
import argparse
import numpy as np
import datetime
from torchcore.tools import Logger
import torchvision
import math
import tqdm
import time

from rcnn_config import config
from torchvision.ops import RoIAlign

import torch
import torch.optim as optim

from torchcore.dnn import trainer

import matplotlib.pyplot as plt
from bbox_pred_net import BBoxPredNet, BBoxHead
from bbox_pred_net_with_attention import BBoxPredNetWithAttention, BBoxHeadAttention
from bbox_dataset import BBoxDataset,BBoxCollateFn
from attention_head import AttentionHead

def parse_commandline():
    parser = argparse.ArgumentParser(description="Training the Model")
    parser.add_argument('-b','--batch_size',help='Batch size per step per gpu', required=True, type=int)
    parser.add_argument('-a','--accumulation_step',help='Accumulate size', required=False, default=1, type=int)
    parser.add_argument('-t','--tag',help='Model tag', required=False)
    parser.add_argument('--config',help='config name', required=False, default=None)
    parser.add_argument('--resume',help='resume the model', action='store_true', required=False)
    parser.add_argument('--load_model_path',help='load weights for the model', default=None, required=False)
    parser.add_argument('--evaluate',help='Do you just want to evaluate the model', action='store_true', required=False)
    parser.add_argument('--multi_scale',help='do you want to do multi scale training', action='store_true', required=False)
    parser.add_argument('--dataset', help='The dataset we are going to use', default='coco_person')
    parser.add_argument('--lr',help='initial learning rate', required=False, type=float, default=None)
    return parser.parse_args()


class my_trainer(trainer):
    def __init__( self, cfg, model, device, trainset, testset=None, dataset_name='modanet', benchmark=None, criterias=None, val_dataset=None, clip_gradient=None, tag='' ):
        self._cfg = cfg
        self._device = device
        self._optimizer = None
        self._model = model

        self._trainset = trainset
        self._testset = testset
        self._benchmark = benchmark
        self._criterias = criterias
        self._dataset_name = dataset_name
        self._epoch = 0
        self._val_dataset = val_dataset
        self._niter = cfg.optimizer.n_iter

        self._clip_gradient = clip_gradient
        self._tag = tag

        self._set_optimizer()
        self._set_scheduler()
        if cfg.resume:
            path = os.path.join(os.path.dirname(cfg.path.CHECKPOINT), '{}_last.pth'.format(self._tag)) 
            self.resume_training(path, device)

        model.to(device)
        self._model = model

        self.init_logger()

    def train( self ):
        #if self._testset is not None :
        #    self._validate()
        #    self._validate_torchvision()

        for i in range( self._epoch+1, self._niter+1 ):
            print("Epoch %d/%d" % (i,self._niter))
            self._logger.info('epoch {}\n'.format(i))
            self._train()

            if self._testset is not None and i%1 == 0 :
                self._validate()
            self._epoch = i
            self.save_training(self._cfg.path.CHECKPOINT.format(self._epoch))
            if hasattr(self, '_scheduler'):
                self._scheduler.step()

    def validate_onece(self):
        self._validate()

    def _validate( self ):
        print('start to validate')
        self._model.eval()

        ious = []
        ious_dict = {}
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(tqdm.tqdm(self._testset, 'evaluating')):
                inputs = self._set_device( inputs )
                targets = self._set_device(targets)
                output = self._model( inputs)
                im_id = targets['image_id']
                for pred, target in zip(output['target_box'], targets['target_box']):
                    iou = self.cal_iou(pred, target)
                    if torch.is_tensor(iou):
                        iou = iou.detach().cpu().numpy()
                    ious_dict[im_id] = iou
                    ious.append(iou)
                #if idx > 20:
                #    break
        ious = np.array(ious)
        print('mean iou is {}'.format(np.mean(ious)))
        self.save_hist(ious)
    
    def save_hist(self, ious):
        plt.figure()
        plt.xlabel('IoU')
        plt.ylabel('sample number')
        #bins = np.linspace(0,1,11)
        bins = 20
        #plt.ylim(0, 50)
        plt.hist(ious, bins=bins)
        plt.savefig('hist_{}.jpg'.format(self._tag))

    
    def cal_iou(self, box1, box2):
        x1 = max(box1[0], box2[0])
        y1 = max(box1[1], box2[1])
        x2 = min(box1[2], box2[2])
        y2 = min(box1[3], box2[3])
        if x2<=x1 or y2<=y1:
            return 0
        inter = (x2-x1) * (y2-y1)
        box1_area = (box1[2]-box1[0]) * (box1[3]-box1[1])
        box2_area = (box2[2]-box2[0]) * (box2[3]-box2[1])
        return inter / (box1_area+box2_area-inter)


    def _train( self ):
        self._model.train()

        loss_values = []
        average_losses = {}

        self._optimizer.zero_grad()
        back_time = 0
        forward_time = 0
        for idx, (inputs, targets) in enumerate(tqdm.tqdm(self._trainset, desc='Training')):

            f_s = time.time()
            inputs = self._set_device( inputs )
            targets = self._set_device( targets )

            loss_dict = self._model(inputs, targets)
            forward_time += time.time() - f_s

            # add the losses for each part
            loss_sum=0
            for single_loss in loss_dict:
                loss_sum = loss_sum + loss_dict[single_loss]
                if single_loss in average_losses:
                    average_losses[single_loss]+= loss_dict[single_loss].item()
                else:
                    average_losses[single_loss] = loss_dict[single_loss].item()

            loss_sum_num = loss_sum.item()
            if not math.isfinite(loss_sum_num):
                self._optimizer.zero_grad()
                print('wrong targets:',targets)
                print("Loss is {}, skip this batch".format(loss_sum_num))
                print(loss_dict)
                continue

            b_s = time.time()
            # Computing gradient and do SGD step
            loss_sum.backward()

            if self._clip_gradient is not None:
                torch.nn.utils.clip_grad_norm_(model.parameters(), self._clip_gradient)
            #torch.cuda.synchronize()
            if idx % self._cfg.accumulation_step == 0:
                self._optimizer.step()
                self._optimizer.zero_grad()
            loss_values.append( loss_sum_num )
            back_time += time.time() - b_s

            print_interval = 100
            if idx%print_interval == 0:
                self._logger.info('{} '.format(idx+1))
                loss_str = ''
                for loss in average_losses:
                    if idx==0:
                        loss_num = average_losses[loss]
                    else:
                        loss_num = average_losses[loss] / print_interval
                    self._logger.info('{} '.format(loss_num))
                    loss_str += (' {} loss:{}, '.format(loss, loss_num))
                print(loss_str[:-2])
                average_losses = {}
                self._logger.info('\n')
            #if idx>10:
            #    break
            

        print('Average loss : ', np.mean(loss_values))

    def _set_device( self, blobs ):
        if type(blobs) == list:
            for i in range(len(blobs)):
                blobs[i] = self._set_device(blobs[i])
        elif type(blobs) == dict:
            for key, data in blobs.items():
                blobs[key] = self._set_device(data)
        elif torch.is_tensor(blobs):
            blobs = blobs.to(self._device)
        return blobs

    def _set_scheduler(self):
        scheduler = self._cfg.scheduler
        if scheduler.type == 'multi_step':
            self._scheduler = optim.lr_scheduler.MultiStepLR( self._optimizer,
                                                            milestones=scheduler['milestones'],
                                                            gamma=scheduler.get('gamma', 0.1))
        else:
            raise ValueError('unknow scheduler {}'.format(scheduler.type))

    def init_logger(self):
        train_path = self._cfg.path['LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=train_path, console=False, console_formatter=console_formatter)

        print('Loss log path is: {}'.format(train_path))

    def save_training(self, path, to_print=True):
        if isinstance(model, torch.nn.DataParallel):
            state_dict = self._model.module.state_dict()
        else:
            state_dict =self._model.state_dict()
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': state_dict,
            'optimizer_state_dict': self._optimizer.state_dict(),
            'scheduler':self._scheduler.state_dict(),
            'niter':self._niter,
            'config':self._cfg
        }, path)
        folder = os.path.dirname(path)
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': state_dict,
            'optimizer_state_dict': self._optimizer.state_dict(),
            'scheduler':self._scheduler.state_dict(),
            'niter':self._niter,
            'config':self._cfg
        }, os.path.join(folder, '{}_last.pth'.format(self._tag)))
        if to_print:
            print('The checkpoint has been saved to {}'.format(path))

    def resume_training(self, path, device, to_print=True):
        checkpoint = torch.load(path, map_location=device)
        self._epoch = checkpoint['epoch']
        self._model.load_state_dict(checkpoint['model_state_dict'])
        self._optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        self.move_optimizer_to_device(self._optimizer, device)
        self._niter = self._niter
        if 'scheduler' in checkpoint:
            self._scheduler.load_state_dict(checkpoint['scheduler'])
        if to_print:
            print('Chekpoint has been loaded from {}'.format(path))
    
    def move_optimizer_to_device(self, optimizer, device):
        for state in optimizer.state.values():
            for k, v in state.items():
                if isinstance(v, torch.Tensor):
                    state[k] = v.to(device)

def load_checkpoint(model, path, device, to_print=True):
    #checkpoint = torch.load(path)
    state_dict_ = torch.load(path, map_location=device)['model_state_dict']
    state_dict = {}
    for k in state_dict_:
        if k.startswith('module') and not k.startswith('module_list'):
            state_dict[k[7:]] = state_dict_[k]
        else:
            state_dict[k] = state_dict_[k]
    model.load_state_dict(state_dict, strict=True )
    if to_print:
        print('Chekpoint has been loaded from {}'.format(path))

def get_model(cfg):
    backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet50', pretrained=True)
    attention_head = AttentionHead(cfg.attention_head)
    head = BBoxHeadAttention(cfg.head)
    roi_pooler = RoIAlign(cfg.roi_pooler.output_size, spatial_scale=1.0/cfg.roi_pooler.stride, sampling_ratio=4, aligned=True)
    model = BBoxPredNetWithAttention(backbone=backbone, attention_head=attention_head, head=head, roi_pooler=roi_pooler)

    return model
    


if __name__=="__main__" :
    args = parse_commandline()
    clip_gradient = None
    params = {}
    if args.config is None:
        params['config'] = 'bbox_attention'
    else:
        params['config'] = args.config
    params['tag'] = args.tag
    params['path'] = {}
    batch_size_per_gpu_per_accumulation = args.batch_size

    params['path']['project'] = os.path.expanduser('~/Vision/data')

    cfg = config( params['config'], params['path']['project'] )
    cfg.update( args )

    cfg.min_size = 416
    cfg.max_size = 416

    if args.lr is not None:
        cfg.optimizer.lr = args.lr

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], args.dataset, model_hash='frcnn' )
    print(cfg)


    anno_path = os.path.expanduser('~/data/annotations/modanet2018_instances_revised.pkl')
    #anno_path_val = os.path.expanduser('~/data/annotations/modanet2018_instances_val.pkl')
    im_root = os.path.expanduser('~/data/datasets/modanet/Images')
    boxes_path = 'modanet_boxes.pkl'

    #transform = BBoxTransform(max_side=416)
    dataset = BBoxDataset(boxes_path, anno_path, 'train', im_root, transforms=None)
    test_dataset = BBoxDataset(boxes_path, anno_path, 'test', im_root, transforms=None)

    if args.multi_scale:
        max_size=[384, 416,448]
    else:
        max_size = 416
    collate_fn = BBoxCollateFn(max_size=max_size)
    collate_fn_test = BBoxCollateFn(max_size=416)

    

    if args.evaluate:
        data_loader = None
    else:
        data_loader = torch.utils.data.DataLoader(
                dataset, 
                batch_size=batch_size_per_gpu_per_accumulation, 
                shuffle=True, 
                num_workers=4,
                pin_memory=True,
                drop_last=False,
                collate_fn=collate_fn)
    test_data_loader = torch.utils.data.DataLoader(
            test_dataset, 
            batch_size=batch_size_per_gpu_per_accumulation, 
            shuffle=False, 
            num_workers=4,
            pin_memory=True,
            drop_last=False,
            collate_fn=collate_fn_test)

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    model = get_model(cfg )

    dataset_name = args.dataset

    t = my_trainer( cfg, model, device, data_loader, testset=test_data_loader, dataset_name=dataset_name, benchmark=None, val_dataset=None, clip_gradient=clip_gradient, tag=args.tag )

    if not args.evaluate:
        t.train()
    else:
        if args.load_model_path is not None:
            load_checkpoint(model, args.load_model_path, device, to_print=True)
            t.validate_onece()
