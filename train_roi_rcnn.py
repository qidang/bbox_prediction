#import _init
import os
import argparse
import platform
import numpy as np
import datetime
import matplotlib.pyplot as plt

from torchcore.tools import Logger
import torchvision
import math
from torch import nn
import json
import tqdm
import time
from pprint import pprint

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
from torchcore.data.transforms import RandomMirror

from rcnn_config import config

from torchcore.data.datasets import ModanetDataset, COCOPersonDataset, COCODataset, COCOTorchVisionDataset, FashionPediaDataset
from torchcore.data.datasets import MixDataset
from torchcore.data.sampler import MixBatchSampler
from torchcore.data.collate import CollateFnRCNN, collate_fn_torchvision

import torch
import torchvision.transforms as transforms
import torch.optim as optim

from torchcore.dnn import trainer
from torchcore.dnn.networks.roi_net import RoINet
from torchcore.dnn.networks.rpn import MyAnchorGenerator, MyRegionProposalNetwork
from torchcore.dnn.networks.heads import RPNHead
from torchcore.data.transforms import Compose, RandomCrop, RandomScale, RandomMirror, AddSurrandingBox, AddPersonBox
from torchcore.dnn.networks.tools.data_parallel import DataParallel

from torchcore.dnn.networks.pooling.roi_align_fpn import RoiAliagnFPN

from torchcore.dnn.networks.mix_rcnn import MixRCNN
from torchcore.dnn.networks.mix_rcnn_bbox import MixRCNNBBox
from torchcore.dnn.networks.garment_box_pred import GarmentBoxPredNet
from torchcore.dnn.networks.pooling.in_target_roi_pool import TargetInRoIPool
from torchcore.dnn.networks.detection_heads.faster_rcnn_heads import FasterRCNNHeads
from torchcore.dnn.networks.detection_heads.roi_centernet_heads import RoICenterNetHeads
from torchcore.dnn.networks.tools.roi_detection_target_converter import RoIDetectionTargetsConverter, RoIRPNInputConverter
from torchcore.dnn.networks.detection_heads.roi_faster_rcnn_heads import RoIFasterRCNNHeads
from torchcore.dnn.networks.mix_rcnn_bbox_det import MixRCNNBBoxDetector
from torchcore.dnn.networks.roi_rcnn import RoiRCNN

from bbox_pred_net import BBoxHead
from torchcore.data.collate import CollateFnPadding
from torchcore.data.transforms import RandomMirror, RandomScale, Compose, RandomAbsoluteScale
from torchcore.data.datasets.bbox_dataset import BBoxDataset

from torchvision.ops import box_iou

def parse_commandline():
    parser = argparse.ArgumentParser(description="Training the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    parser.add_argument('-b','--batch_size',help='Batch size per step per gpu', required=True, type=int)
    parser.add_argument('-a','--accumulation_step',help='Accumulate size', required=False, default=1, type=int)
    parser.add_argument('-t','--tag',help='Model tag', required=False)
    parser.add_argument('--resume',help='resume the model', action='store_true', required=False)
    parser.add_argument('--load_model_path',help='load weights for the model', default=None, required=False)
    parser.add_argument('--evaluate',help='Do you just want to evaluate the model', action='store_true', required=False)
    parser.add_argument('--dataset', help='The dataset we are going to use', default='coco_person')
    parser.add_argument('--roi_w',help='The width of roi align', required=False, type=int, default=None)
    parser.add_argument('--roi_h',help='The width of roi align', required=False, type=int, default=None)
    parser.add_argument('--second_loss_weight',help='The weight of second head loss', required=False, type=float, default=None)
    parser.add_argument('--third_loss_weight',help='The weight of third head loss', required=False, type=float, default=None)
    parser.add_argument('--expand_ratio',help='The expand ritio for the boxes', required=False, type=float, default=0)
    parser.add_argument('--lr',help='initial learning rate', required=False, type=float, default=None)
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return parser.parse_args()


class my_trainer(trainer):
    def __init__( self, cfg, model, device, trainset, testset=None, dataset_name='modanet', benchmark=None, criterias=None, val_dataset=None, clip_gradient=None, tag='' ):
        self._cfg = cfg
        self._device = device
        self._optimizer = None
        self._model = model

        self._trainset = trainset
        self._testset = testset
        self._benchmark = benchmark
        self._criterias = criterias
        self._dataset_name = dataset_name
        self._epoch = 0
        self._val_dataset = val_dataset
        self._niter = cfg.optimizer.n_iter

        self._clip_gradient = clip_gradient
        self._tag = tag

        self._set_optimizer()
        self._set_scheduler()
        if cfg.resume:
            path = os.path.join(os.path.dirname(cfg.path.CHECKPOINT), '{}_last.pth'.format(self._tag)) 
            self.resume_training(path, device)

        #model = DataParallel(model,
        #    device_ids=None, 
        #    chunk_sizes=None)
        model.to(device)
        self._model = model

        self.init_logger()

    def train( self ):
        #if self._testset is not None :
        #    self._validate()
        #    self._validate_torchvision()

        for i in range( self._epoch+1, self._niter+1 ):
            print("Epoch %d/%d" % (i,self._niter))
            self._logger.info('epoch {}\n'.format(i))
            self._train()

            if self._testset is not None and i%1 == 0 :
                self._validate_garments()
            self._epoch = i
            self.save_training(self._cfg.path.CHECKPOINT.format(self._epoch))
            if hasattr(self, '_scheduler'):
                self._scheduler.step()

    def validate_onece(self):
        self._validate()

    def _validate_garments( self ):
        print('start to validate')
        self._model.test_mode = 'second'
        self._model.eval()

        results = []
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(tqdm.tqdm(self._testset, 'evaluating')):
                inputs = self._set_device( inputs )
                targets = self._set_device( targets )
                output = self._model( inputs, targets)
                batch_size = len(output['boxes'])
                #for i, im in enumerate(output):
                for i in range(batch_size):
                    if len(output['boxes'][i]) == 0:
                        continue
                    # convert to xywh
                    output['boxes'][i][:,2] -= output['boxes'][i][:,0]
                    output['boxes'][i][:,3] -= output['boxes'][i][:,1]
                    for j in range(len(output['boxes'][i])):
                        results.append({'image_id':int(targets[i]['image_id']), 
                                        'category_id':output['labels'][i][j].cpu().numpy().round(decimals=2).tolist(), 
                                        'bbox':output['boxes'][i][j].cpu().numpy().round(decimals=2).tolist(), 
                                        'score':output['scores'][i][j].cpu().numpy().round(decimals=2).tolist()})
                #output = self._model['net']( inputs, just_embedding=True) # debug
                #bench.update( targets, output )
                #if idx>=11:
                #    break
                
        result_path = '{}temp_result.json'.format(self._tag)
        with open(result_path,'w') as f:
            json.dump(results,f)
        self.eval_result(result_path, dataset=self._dataset_name)

    def _validate( self ):
        print('start to validate')
        self.cal_inside = True
        self._model.eval()

        ious = []
        ious_dict = {}
        inside_ous = [[] for i in range(13)]
        #labels_all = [[] for i in range(13)]
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(tqdm.tqdm(self._testset, 'evaluating')):
                inputs = self._set_device( inputs )
                targets = self._set_device(targets)
                output = self._model( inputs)
                for pred, target in zip(output['target_box'], targets):
                    im_id = target['image_id']
                    target_box = target['target_box']
                    #assert len(pred) == 1
                    #if len(pred) != 1:
                    #    print('{} prediction'.format(len(pred)))
                    for single_pred in pred:
                        iou = self.cal_iou(single_pred, target_box)
                        if torch.is_tensor(iou):
                            iou = iou.detach().cpu().numpy()
                        ious_dict[im_id] = iou
                        ious.append(iou)
                if self.cal_inside:
                    for target in targets:
                        for bboxes, labels, target_box in zip(target['boxes'], target['labels'], output['target_box']):
                            keep = labels>=0
                            labels = labels[keep]
                            bboxes = bboxes[keep]
                            #print(target_box)
                            #print(bboxes)
                            for pred_t_box in target_box:
                                inside_ou = self.cal_inside_box_union(pred_t_box, bboxes)
                                if torch.is_tensor(inside_ou):
                                    inside_ou = inside_ou.detach().cpu().numpy()
                                #inside_ous.append(inside_ou)
                                if torch.is_tensor(labels):
                                    labels = labels.detach().cpu().numpy()
                                for label, ou in zip(labels, inside_ou):
                                    assert label > 0
                                    inside_ous[label-1].append(ou)

                #if idx > 150:
                #    break
        ious = np.array(ious)
        print('mean iou is {}'.format(np.mean(ious)))
        print('mean ou is {}'.format(np.mean(np.concatenate(inside_ous))))
        self.save_hist(ious)
        self.save_ou_hist(inside_ous)

    def _validate_boxes( self ):
        print('start to validate boxes')
        self.cal_inside = True
        self._model.eval()
        self._model.test_mode = 'second'

        all = 0
        ious = []
        labels = []
        labels_prop = []
        ious_prop = []
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(tqdm.tqdm(self._testset, 'evaluating')):
                inputs = self._set_device( inputs )
                targets = self._set_device(targets)
                output = self._model( inputs)
                #print(output)
                for pred, target in zip(output['boxes'], targets):
                    #im_id = target['image_id']
                    target_boxes = target['boxes']
                    iou_mat = box_iou(pred, target_boxes)
                    val, ind = iou_mat.max(dim=0)
                    val_proposal, ind_proposal = iou_mat.max(dim=1)
                    valid_ind = val_proposal >= 0.4
                    val_proposal = val_proposal[valid_ind]
                    ious_prop.append(val_proposal)
                    labels_prop.append(target['labels'][ind_proposal[valid_ind]])
                    ious.append(val)
                    all += len(target['boxes'])
                    labels.append(target['labels'])
                    #print(target_boxes)
                    #print(pred)
                    #print(iou_mat.shape)
                    #print(val.shape)
                #if idx > 50:
                #    break
        iou_all = torch.cat(ious).detach().cpu().numpy()
        labels = torch.cat(labels).detach().cpu().numpy()
        labels_prop = torch.cat(labels_prop).detach().cpu().numpy()
        ious_prop = torch.cat(ious_prop).detach().cpu().numpy()
        #self.save_hist(iou_all)
        self.save_hist_by_category(iou_all, labels, 'How many targets are overlaped by proposal', 'IoU', 'Sample number')
        self.save_hist_by_category(ious_prop, labels_prop,'Proposal with minumum 0.4 IoU distribution', 'IoU', 'Sample number')
        self.save_score_distribution(iou_all)
        print('Everage IoU is {}'.format(iou_all.mean()))
        
    def save_score_distribution(self,ious):
        total = len(ious)
        nums = []
        thres = []
        for iou_thre in np.arange(0,1, 0.05):
            hit_num = (ious>=iou_thre).sum()
            nums.append( hit_num / total)
            thres.append(iou_thre)

        plt.figure(figsize=(10,10))
        plt.xlabel('IoU threshold')
        plt.ylabel('Hit percentage')
        plt.plot(thres, nums, 's-', color='r')
        for a, b in zip(thres, nums):
            plt.text(a, b, '{:.3f}'.format(b), ha='center', va='bottom', fontsize=10)

        plt.title('Bbox hit percentage vs IoU threshould')
        plt.savefig('linechart_{}.jpg'.format(self._tag))
            

                #    break
    
    def save_hist(self, ious):
        plt.figure()
        plt.xlabel('IoU')
        plt.ylabel('sample number')
        #bins = np.linspace(0,1,11)
        bins = 20
        #plt.ylim(0, 50)
        plt.hist(ious, bins=bins)
        plt.savefig('hist_{}.jpg'.format(self._tag))

    def save_hist_by_category(self, ious, cat_lables, title, xlabel, ylabel, log_scale=False):
        fig, plots = plt.subplots(nrows=3, ncols=5, figsize=(15, 9))
        fig.suptitle(title)
        colors = plt.rcParams["axes.prop_cycle"]()

        labels = ['bag','belt','boots','footwear','outer', 'dress','sunglasses', 'pants','top','shorts','skirt','headwear', 'scarf&tie']
        bins = 20
        bins = np.arange(0,1.1,0.1)
        count = 0
        for i, row in enumerate(plots):
            for j, ax in enumerate(row):
                ax.set_xlabel(xlabel)
                ax.set_ylabel(ylabel)
                if log_scale:
                    ax.set_yscale('log')
                ind = cat_lables == count+1
                label = labels[count]
                iou_cat = ious[ind]
                c = next(colors)["color"]
                ax.hist(iou_cat, bins=bins, color=c)
                ax.set_title(label)
                
                count += 1
                if count >= len(labels):
                    j += 1
                    while j < len(row):
                        row[j].axis('off')
                        j+=1

                    break
        fig.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
        plt.savefig('hist_iou_{}.jpg'.format(self._tag))

    def save_ou_hist_stack(self, ous):
        plt.figure()
        plt.xlabel('Inside of Union')
        plt.ylabel('Sample number')
        #bins = np.linspace(0,1,11)
        bins = 20
        #plt.ylim(0, 50)
        plt.hist(ous, stacked=True, bins=bins)
        labels = ['bag','belt','boots','footwear','outer', 'dress','sunglasses', 'pants','top','shorts','skirt','headwear', 'scarf&tie']
        plt.legend(labels)
        plt.savefig('hist_ou_{}.jpg'.format(self._tag))

    def save_ou_hist(self, ous):
        fig, plots = plt.subplots(nrows=3, ncols=5, figsize=(15, 9))
        colors = plt.rcParams["axes.prop_cycle"]()

        labels = ['bag','belt','boots','footwear','outer', 'dress','sunglasses', 'pants','top','shorts','skirt','headwear', 'scarf&tie']
        bins = 20
        bins = np.arange(0,1.1,0.1)
        count = 0
        for i, row in enumerate(plots):
            for j, ax in enumerate(row):
                ax.set_xlabel('Inside of Union')
                ax.set_ylabel('Sample number')
                ax.set_yscale('log')
                label = labels[count]
                c = next(colors)["color"]
                ax.hist(ous[count], bins=bins, color=c)
                ax.set_title('Inside of Uninon for {}'.format(label))
                
                count += 1
                if count >= len(labels):
                    j += 1
                    while j < len(row):
                        row[j].axis('off')
                        j+=1

                    break



        #plt.legend(labels)
        fig.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
        plt.savefig('hist_ou_{}.jpg'.format(self._tag))

    
    def cal_iou(self, box1, box2):
        x1 = max(box1[0], box2[0])
        y1 = max(box1[1], box2[1])
        x2 = min(box1[2], box2[2])
        y2 = min(box1[3], box2[3])
        if x2<=x1 or y2<=y1:
            return 0
        inter = (x2-x1) * (y2-y1)
        box1_area = (box1[2]-box1[0]) * (box1[3]-box1[1])
        box2_area = (box2[2]-box2[0]) * (box2[3]-box2[1])
        return inter / (box1_area+box2_area-inter)

    def cal_inside_box_union(self,target_box, garment_boxes):
        x1 = torch.maximum(garment_boxes[:,0], target_box[0])
        y1 = torch.maximum(garment_boxes[:,1], target_box[1])
        x2 = torch.minimum(garment_boxes[:,2], target_box[2])
        y2 = torch.minimum(garment_boxes[:,3], target_box[3])
        w = (x2-x1).clamp_min(0)
        h = (y2-y1).clamp_min(0)
        inter = w*h
        box_area = (garment_boxes[:,2]-garment_boxes[:,0]) * (garment_boxes[:,3]-garment_boxes[:,1])
        return inter / box_area
    def validate_mix( self ):
        print('start to validate')
        self._model.eval()
        modes = ['first', 'second']

        debug = True
        for mode, testset, dataset_name in zip(modes, self._testset, self._dataset_name):
            if debug:
                debug = False
                continue
            self._model.test_mode = mode
            results = []
            with torch.no_grad() :
                for idx,(inputs, targets) in enumerate(tqdm.tqdm(testset, 'evaluating')):
                    inputs = self._set_device( inputs )
                    output = self._model( inputs)
                    batch_size = len(output['boxes'])
                    #for i, im in enumerate(output):
                    for i in range(batch_size):
                        if len(output['boxes'][i]) == 0:
                            continue
                        # convert to xywh
                        output['boxes'][i][:,2] -= output['boxes'][i][:,0]
                        output['boxes'][i][:,3] -= output['boxes'][i][:,1]
                        for j in range(len(output['boxes'][i])):
                            results.append({'image_id':int(targets[i]['image_id']), 
                                            'category_id':output['labels'][i][j].cpu().numpy().round(decimals=2).tolist(), 
                                            'bbox':output['boxes'][i][j].cpu().numpy().round(decimals=2).tolist(), 
                                            'score':output['scores'][i][j].cpu().numpy().round(decimals=2).tolist()})
                    #if idx>=11:
                    #    break
                    
            result_path = '{}temp_result.json'.format(self._tag)
            with open(result_path,'w') as f:
                json.dump(results,f)
            self.eval_result(result_path, dataset=dataset_name)

    def eval_result(self, result_path, dataset='coco_person'):
        if dataset not in ['coco','coco_person', 'modanet', 'fashionpedia']:
            raise ValueError('only support coco, coco_person and modanet dataset')
        if dataset == 'coco_person':
            gt_json=os.path.expanduser('~/data/datasets/COCO/annotations/instances_val2014.json')
        elif dataset == 'coco':
            gt_json=os.path.expanduser('~/data/datasets/COCO/annotations/instances_val2017.json')
        elif dataset == 'fashionpedia':
            gt_json=os.path.expanduser('~/data/datasets/Fashionpedia/annotations/instances_attributes_val2020.json')
        elif dataset == 'modanet':
            gt_json=os.path.expanduser('~/data/datasets/modanet/Annots/modanet_instances_val.json')


        # we need to map the category ids back
        if dataset == 'coco':
            print('revise coco dataset label to gt')
            cat_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 67, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 84, 85, 86, 87, 88, 89, 90]
            with open(result_path) as f:
                results = json.load(f)
                for result in results:
                    temp_id = result['category_id']
                    result['category_id'] = cat_ids[temp_id-1]
            with open(result_path,'w') as f:
                json.dump(results,f)
        if dataset == 'fashionpedia':
            print('revise fashionpedia dataset label to gt')
            with open(result_path) as f:
                results = json.load(f)
                for result in results:
                    temp_id = result['category_id']
                    result['category_id'] = temp_id-1
            with open(result_path,'w') as f:
                json.dump(results,f)
        annType = 'bbox'
        cocoGt=COCO(gt_json)
        cocoDt=cocoGt.loadRes(result_path)

        imgIds=sorted(cocoGt.getImgIds())

        # running evaluation
        cocoEval = COCOeval(cocoGt,cocoDt,annType)
        if dataset == 'coco_person':
            cocoEval.params.catIds = [1]
        cocoEval.params.imgIds = imgIds
        cocoEval.evaluate()
        cocoEval.accumulate()
        cocoEval.summarize()

    def _train( self ):
        self._model.train()

        loss_values = []
        average_losses = {}

        self._optimizer.zero_grad()
        back_time = 0
        forward_time = 0
        for idx, (inputs, targets) in enumerate(tqdm.tqdm(self._trainset, desc='Training')):
            #print('inputs:', inputs)
            #print('targets:', targets)

            f_s = time.time()
            inputs = self._set_device( inputs )
            targets = self._set_device( targets )
            #print('input device:', inputs[0].device)

            loss_dict = self._model(inputs, targets)
            forward_time += time.time() - f_s

            # add the losses for each part
            #loss_sum = sum(loss for loss in loss_dict.values())
            loss_sum=0
            for single_loss in loss_dict:
                loss_sum = loss_sum + loss_dict[single_loss]
                if single_loss in average_losses:
                    average_losses[single_loss]+= loss_dict[single_loss].item()
                else:
                    average_losses[single_loss] = loss_dict[single_loss].item()

            loss_sum_num = loss_sum.item()
            if not math.isfinite(loss_sum_num):
                #print("Loss is {}, stopping training".format(loss_sum))
                self._optimizer.zero_grad()
                print('wrong targets:',targets)
                print("Loss is {}, skip this batch".format(loss_sum_num))
                print(loss_dict)
                continue
                #sys.exit(1)

            b_s = time.time()
            # Computing gradient and do SGD step
            loss_sum.backward()

            if self._clip_gradient is not None:
                torch.nn.utils.clip_grad_norm_(model.parameters(), self._clip_gradient)
            #torch.cuda.synchronize()
            if idx % self._cfg.accumulation_step == 0:
                self._optimizer.step()
                self._optimizer.zero_grad()
            loss_values.append( loss_sum_num )
            back_time += time.time() - b_s

            print_interval = 1000
            if idx%print_interval == 0:
                self._logger.info('{} '.format(idx+1))
                loss_str = ''
                for loss in average_losses:
                    if idx==0:
                        loss_num = average_losses[loss]
                    else:
                        loss_num = average_losses[loss] / print_interval
                    self._logger.info('{} '.format(loss_num))
                    loss_str += (' {} loss:{}, '.format(loss, loss_num))
                print(loss_str[:-2])
                average_losses = {}
                self._logger.info('\n')
                #print(self._model.total_time)
                #self._model.reset_time()
                #print('forward time:', forward_time)
                #forward_time = 0
                #print('back time:', back_time)
                #back_time = 0
            #if idx>10:
            #    break
            

        print('Average loss : ', np.mean(loss_values))

    def _set_device( self, blobs ):
        if type(blobs) == list:
            for i in range(len(blobs)):
                blobs[i] = self._set_device(blobs[i])
        elif type(blobs) == dict:
            for key, data in blobs.items():
                blobs[key] = self._set_device(data)
        elif torch.is_tensor(blobs):
            blobs = blobs.to(self._device)
        return blobs

    def _set_scheduler(self):
        scheduler = self._cfg.scheduler
        if scheduler.type == 'multi_step':
            self._scheduler = optim.lr_scheduler.MultiStepLR( self._optimizer,
                                                            milestones=scheduler['milestones'],
                                                            gamma=scheduler.get('gamma', 0.1))
        else:
            raise ValueError('unknow scheduler {}'.format(scheduler.type))

    def init_logger(self):
        train_path = self._cfg.path['LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=train_path, console=False, console_formatter=console_formatter)

        print('Loss log path is: {}'.format(train_path))

    def save_training(self, path, to_print=True):
        if isinstance(model, torch.nn.DataParallel):
            state_dict = self._model.module.state_dict()
        else:
            state_dict =self._model.state_dict()
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': state_dict,
            'optimizer_state_dict': self._optimizer.state_dict(),
            'scheduler':self._scheduler.state_dict(),
            'niter':self._niter,
            'config':self._cfg
        }, path)
        folder = os.path.dirname(path)
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': state_dict,
            'optimizer_state_dict': self._optimizer.state_dict(),
            'scheduler':self._scheduler.state_dict(),
            'niter':self._niter,
            'config':self._cfg
        }, os.path.join(folder, '{}_last.pth'.format(self._tag)))
        if to_print:
            print('The checkpoint has been saved to {}'.format(path))

    def resume_training(self, path, device, to_print=True):
        checkpoint = torch.load(path, map_location=device)
        self._epoch = checkpoint['epoch']
        self._model.load_state_dict(checkpoint['model_state_dict'])
        self._optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        self.move_optimizer_to_device(self._optimizer, device)
        self._niter = self._niter
        if 'scheduler' in checkpoint:
            self._scheduler.load_state_dict(checkpoint['scheduler'])
        if to_print:
            print('Chekpoint has been loaded from {}'.format(path))
    
    def move_optimizer_to_device(self, optimizer, device):
        for state in optimizer.state.values():
            for k, v in state.items():
                if isinstance(v, torch.Tensor):
                    state[k] = v.to(device)

def load_checkpoint(model, path, device, to_print=True):
    #checkpoint = torch.load(path)
    state_dict_ = torch.load(path, map_location=device)['model_state_dict']
    state_dict = {}
    for k in state_dict_:
        if k.startswith('module') and not k.startswith('module_list'):
            state_dict[k[7:]] = state_dict_[k]
        else:
            state_dict[k] = state_dict_[k]
    model.load_state_dict(state_dict, strict=True )
    #self._epoch = checkpoint['epoch']
    #self._model.load_state_dict(checkpoint['model_state_dict'])
    #self._optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    if to_print:
        print('Chekpoint has been loaded from {}'.format(path))

def get_fovea_model(cfg):
    #backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet101', pretrained=True)
    backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet50', pretrained=True)
    heads = {}
    #anchor_generator = MyAnchorGenerator(sizes=((32,),(64,),(128,),(256,),(512,)), aspect_ratios=(0.5,1.0,2.0))
    anchor_generator = MyAnchorGenerator(sizes=((32,),(64,),(128,),(256,),), aspect_ratios=(0.5,1.0,2.0))
    num_anchors_per_location = anchor_generator.num_anchors_per_location()
    assert all(num_anchors_per_location[0] == item for item in num_anchors_per_location)
    #print('num anchors per location',num_anchors)
    rpn_head = RPNHead(256, num_anchors=num_anchors_per_location[0])
    rpn = MyRegionProposalNetwork(anchor_generator, rpn_head, cfg.first_head.rpn)
    roi_head = RoINet(cfg.first_head.roi_head)
    first_head = FasterRCNNHeads(cfg.first_head, rpn, roi_head, post_process=False)

    #roi_pooler = ROIAlignBatch(cfg.roi_pooler.pool_h, cfg.roi_pooler.pool_w)
    #roi_pooler = ROIAlign(cfg.roi_pooler.pool_h, cfg.roi_pooler.pool_w)
    roi_pooler_bbox = RoiAliagnFPN(cfg.roi_pooler_bbox.pool_h, cfg.roi_pooler_bbox.pool_w)
    head = BBoxHead(cfg.bbox_head)
    second_head = GarmentBoxPredNet(roi_pooler_bbox, head, targets_converter=None, dataset_label=cfg.bbox_head.dataset_label, feature_name='0',middle_layer=True)

    roi_pooler = TargetInRoIPool(cfg.roi_pooler)
    targets_converter = RoIDetectionTargetsConverter(cfg.targets_converter)
    inputs_converter = RoIRPNInputConverter(cfg.inputs_converter) 

    third_anchor_generator = MyAnchorGenerator(sizes=((32,64,128,),), aspect_ratios=(0.5,1.0,2.0))
    third_num_anchors_per_location = third_anchor_generator.num_anchors_per_location()
    assert all(third_num_anchors_per_location[0] == item for item in third_num_anchors_per_location)
    #print('num anchors per location',num_anchors)
    third_rpn_head = RPNHead(256, num_anchors=third_num_anchors_per_location[0])
    third_rpn = MyRegionProposalNetwork(third_anchor_generator, third_rpn_head, cfg.third_head.rpn)
    third_roi_head = RoINet(cfg.third_head.roi_head)
    third_head = RoIFasterRCNNHeads(cfg.third_head, third_rpn, third_roi_head,just_rpn=True)

    heads['first_head'] = first_head
    heads['second_head'] = second_head
    heads['third_head'] = third_head
    #parts = ['heatmap', 'offset', 'width_height']
    #model = MixRCNNBBox(backbone, heads, test_mode='second',second_loss_weight=cfg.second_loss_weight )
    model = MixRCNNBBoxDetector(backbone, heads, roi_pooler, roi_pool_h=cfg.third_head.roi_pool_h, roi_pool_w=cfg.third_head.roi_pool_w, targets_converter=targets_converter, inputs_converter=inputs_converter, second_loss_weight=cfg.second_loss_weight,third_loss_weight=cfg.third_loss_weight)

    return model

def get_model(cfg):
    #backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet101', pretrained=True)
    backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet50', pretrained=True)
    heads = {}

    roi_pooler = TargetInRoIPool(cfg.roi_pooler)
    targets_converter = RoIDetectionTargetsConverter(cfg.targets_converter)
    inputs_converter = RoIRPNInputConverter(cfg.inputs_converter) 

    anchor_generator = MyAnchorGenerator(sizes=((32,64,128,),), aspect_ratios=(0.5,1.0,2.0))
    num_anchors_per_location = anchor_generator.num_anchors_per_location()
    assert all(num_anchors_per_location[0] == item for item in num_anchors_per_location)
    #print('num anchors per location',num_anchors)
    rpn_head = RPNHead(256, num_anchors=num_anchors_per_location[0])
    rpn = MyRegionProposalNetwork(anchor_generator, rpn_head, cfg.roi_detection_head.rpn)
    roi_head = RoINet(cfg.roi_detection_head.roi_head)
    roi_detection_head = RoIFasterRCNNHeads(cfg.roi_detection_head, rpn, roi_head,just_rpn=cfg.roi_detection_head.just_rpn)


    #parts = ['heatmap', 'offset', 'width_height']
    #model = MixRCNNBBox(backbone, heads, test_mode='second',second_loss_weight=cfg.second_loss_weight )
    model = RoiRCNN(backbone, roi_detection_head, roi_pooler, roi_pool_h=cfg.roi_detection_head.roi_pool_h, roi_pool_w=cfg.roi_detection_head.roi_pool_w, targets_converter=targets_converter, inputs_converter=inputs_converter, roi_post_process=False)

    return model
    


if __name__=="__main__" :
    args = parse_commandline()
    clip_gradient = None
    params = {}
    params['config'] = 'roi_rcnn'
    params['tag'] = args.tag
    params['path'] = {}
    batch_size_per_gpu_per_accumulation = args.batch_size

    params['path']['project'] = os.path.expanduser('~/Vision/data')

    #device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], params['path']['project'] )
    cfg.update( args )

    max_size = 1024
    cfg.min_size = max_size
    cfg.max_size = max_size

    cfg.expand_ratio = args.expand_ratio
    
    if args.lr is not None:
        cfg.optimizer.lr = args.lr

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], args.dataset, model_hash='frcnn' )
    print(cfg)

    transforms = RandomMirror(probability=0.5)
    xyxy = True

    if args.dataset == 'coco_person':
        root = os.path.expanduser('~/data/datasets/COCO')
        anno = os.path.expanduser('~/data/annotations/coco2014_instances_person_all.pkl')
        dataset = COCOPersonDataset(root, anno, part='train2014', transforms=transforms)
        test_dataset = COCOPersonDataset(root, anno, part='val2014', transforms=None)
        cfg.class_num = 1
        cfg.roi_head.class_num = cfg.class_num
    elif args.dataset == 'modanet':
        anno_path = os.path.expanduser('~/data/annotations/modanet2018_instances_revised.pkl')
        root = os.path.expanduser('~/data/datasets/modanet/Images')
        dataset = ModanetDataset(anno=anno_path, part='train', root=root, transforms=transforms) # the default transform in faster rcnn will be used
        test_dataset = ModanetDataset(anno=anno_path, part='val', root=root, transforms=None) # the default transform in faster rcnn will be used
        cfg.class_num = 13
        cfg.roi_head.class_num = cfg.class_num
    elif args.dataset == 'coco':
        anno_path = os.path.expanduser('~/data/annotations/coco2017_instances.pkl')
        root = os.path.expanduser('~/data/datasets/COCO')
        dataset = COCODataset(anno=anno_path, part='train2017', root=root, transforms=transforms, xyxy=xyxy) # the default transform in faster rcnn will be used
        test_dataset = COCODataset(anno=anno_path, part='val2017', root=root, transforms=None, xyxy=xyxy) # the default transform in faster rcnn will be used
        cfg.class_num = 80
        cfg.roi_head.class_num = cfg.class_num
    elif args.dataset == 'fashionpedia':
        anno_path_train = os.path.expanduser('~/data/annotations/fashionpedia_instances_train.pkl')
        anno_path_val = os.path.expanduser('~/data/annotations/fashionpedia_instances_val.pkl')
        root = os.path.expanduser('~/data/datasets/Fashionpedia')
        dataset = FashionPediaDataset(anno=anno_path_train, part='train', root=root, transforms=transforms) # the default transform in faster rcnn will be used
        test_dataset = FashionPediaDataset(anno=anno_path_val, part='val', root=root, transforms=None) # the default transform in faster rcnn will be used
        cfg.class_num = 46
        cfg.roi_head.class_num = cfg.class_num
    elif args.dataset == 'roi_fashionpedia':
        t_person_box = AddPersonBox('fashionpedia_train_person.pkl')
        t_mirrow = RandomMirror(probability=0.5, targets_box_keys=['boxes', 'input_box'])
        t_scale = RandomAbsoluteScale(cfg.max_size/2, cfg.max_size*1.5, targets_box_keys=['boxes', 'input_box'])
        t_crop = RandomCrop(cfg.max_size, box_inside=True, targets_box_keys=['input_box'])
        t_surround_box = AddSurrandingBox()
        #transforms_b = Compose([t_person_box, t_mirrow, t_scale, t_crop, t_surround_box])
        transforms_b = Compose([t_person_box, t_mirrow, t_scale, t_surround_box])

        anno_path_train = os.path.expanduser('~/data/annotations/fashionpedia_instances_train.pkl')
        anno_path_val = os.path.expanduser('~/data/annotations/fashionpedia_instances_val.pkl')
        root = os.path.expanduser('~/data/datasets/Fashionpedia')
        dataset = FashionPediaDataset(anno=anno_path_train, part='train', root=root, transforms=transforms_b) # the default transform in faster rcnn will be used
        cfg.class_num = 46
        cfg.roi_detection_head.roi_head.class_num = cfg.class_num

        test_scale = RandomAbsoluteScale(max_size, max_size, inputs_box_keys=[], targets_box_keys=['boxes'])
        test_surround_box = AddSurrandingBox()
        test_transform = Compose([test_scale, test_surround_box])
        test_dataset = FashionPediaDataset(anno=anno_path_val, part='val', root=root, transforms=test_transform) # the default transform in faster rcnn will be used

        #test_dataset = COCOPersonDataset(root, anno, part='val2014', transforms=None, dataset_label=cfg.first_head.roi_head.dataset_label)
        collate_fn_rcnn = CollateFnPadding()
    elif args.dataset == 'mix_fashionpedia':
        t_mirrow = RandomMirror(probability=0.5)
        t_scale = RandomAbsoluteScale(cfg.max_size/2, cfg.max_size*1.5)
        t_crop = RandomCrop(cfg.max_size, box_inside=True)
        transforms_a = Compose([t_mirrow, t_scale])
        #transforms_a = Compose([t_mirrow, t_scale, t_crop])

        t_person_box = AddPersonBox('fashionpedia_train_person.pkl')
        t_mirrow = RandomMirror(probability=0.5, targets_box_keys=['boxes', 'input_box'])
        t_scale = RandomAbsoluteScale(cfg.max_size/2, cfg.max_size*1.5, targets_box_keys=['boxes', 'input_box'])
        t_crop = RandomCrop(cfg.max_size, box_inside=True, targets_box_keys=['input_box'])
        t_surround_box = AddSurrandingBox()
        #transforms_b = Compose([t_person_box, t_mirrow, t_scale, t_crop, t_surround_box])
        transforms_b = Compose([t_person_box, t_mirrow, t_scale, t_surround_box])

        root = os.path.expanduser('~/data/datasets/COCO')
        anno = os.path.expanduser('~/data/annotations/coco2014_instances_person_all.pkl')
        #model_path = os.path.expanduser('~/Vision/data/embedding/frcnn_coco_person/checkpoints/checkpoints_20201008_rpn_3.pkl')
        dataset_a = COCOPersonDataset(root, anno, part='val2014', transforms=transforms_a)
        anno_path_train = os.path.expanduser('~/data/annotations/fashionpedia_instances_train.pkl')
        anno_path_val = os.path.expanduser('~/data/annotations/fashionpedia_instances_val.pkl')
        root = os.path.expanduser('~/data/datasets/Fashionpedia')
        dataset_b = FashionPediaDataset(anno=anno_path_train, part='train', root=root, transforms=transforms_b) # the default transform in faster rcnn will be used
        cfg.class_num_1 = 1
        cfg.class_num_2 = 46
        cfg.first_head.roi_head.class_num = cfg.class_num_1
        cfg.third_head.roi_head.class_num = cfg.class_num_2

        dataset = MixDataset(dataset_a, dataset_b, shuffle=False, sample=None, add_dataset_label=True)

        test_transform = RandomAbsoluteScale(max_size, max_size, inputs_box_keys=[], targets_box_keys=['boxes'])
        #test_dataset = BBoxDataset(boxes_path, anno_path, 'val', im_root, transforms=test_transform, add_bbox=True, use_modanet_split=True)
        test_dataset = FashionPediaDataset(anno=anno_path_val, part='val', root=root, transforms=test_transform) # the default transform in faster rcnn will be used

        #test_dataset = COCOPersonDataset(root, anno, part='val2014', transforms=None, dataset_label=cfg.first_head.roi_head.dataset_label)
        train_sampler = MixBatchSampler([len(dataset_a), len(dataset_b)], mode='balance', 
                                    batch_size=batch_size_per_gpu_per_accumulation, shuffle=False, drop_last=True)
        collate_fn_rcnn = CollateFnPadding()

    elif args.dataset == 'mix':
        random_mirror = RandomMirror(probability=0.5)
        random_scale = RandomAbsoluteScale(0.25*max_size, 1.25*max_size)
        #random_scale = RandomAbsoluteScale(0.25*max_size, max_size)
        transforms_a = Compose([random_mirror, random_scale])

        random_scale = RandomAbsoluteScale(0.25*max_size, 1.25*max_size, inputs_box_keys=[], targets_box_keys=['input_box', 'target_box', 'boxes'])
        #random_scale = RandomAbsoluteScale(0.25*max_size, max_size, inputs_box_keys=[], targets_box_keys=['input_box', 'target_box', 'boxes'])
        random_mirror = RandomMirror(probability=0.5, inputs_box_keys=[], targets_box_keys=['input_box', 'target_box', 'boxes'])
        transforms_b = Compose([random_mirror, random_scale])
        root = os.path.expanduser('~/data/datasets/COCO')
        anno = os.path.expanduser('~/data/annotations/coco2014_instances_person_all.pkl')
        #model_path = os.path.expanduser('~/Vision/data/embedding/frcnn_coco_person/checkpoints/checkpoints_20201008_rpn_3.pkl')
        dataset_a = COCOPersonDataset(root, anno, part='val2014', transforms=transforms_a)

        anno_path = os.path.expanduser('~/data/annotations/modanet2018_instances_revised.pkl')
        im_root = os.path.expanduser('~/data/datasets/modanet/Images')
        boxes_path = 'modanet_boxes.pkl'
        dataset_b = BBoxDataset(boxes_path, anno_path, 'train', im_root, transforms=transforms_b, add_bbox=True, use_modanet_split=True)
        cfg.class_num_1 = 1
        cfg.class_num_2 = 13
        cfg.first_head.roi_head.class_num = cfg.class_num_1
        cfg.third_head.roi_head.class_num = cfg.class_num_2

        dataset = MixDataset(dataset_a, dataset_b, shuffle=False, sample=None, add_dataset_label=True)

        test_transform = RandomAbsoluteScale(max_size, max_size, inputs_box_keys=[], targets_box_keys=['input_box', 'target_box', 'boxes'])
        test_dataset = BBoxDataset(boxes_path, anno_path, 'val', im_root, transforms=test_transform, add_bbox=True, use_modanet_split=True)

        #test_dataset = COCOPersonDataset(root, anno, part='val2014', transforms=None, dataset_label=cfg.first_head.roi_head.dataset_label)
        train_sampler = MixBatchSampler([len(dataset_a), len(dataset_b)], mode='balance', 
                                    batch_size=batch_size_per_gpu_per_accumulation, shuffle=False, drop_last=True)
        collate_fn_rcnn = CollateFnPadding()
    else:
        print('use mixed dataset')
        root = os.path.expanduser('~/data/datasets/COCO')
        anno = os.path.expanduser('~/data/annotations/coco2014_instances_person_all.pkl')
        #model_path = os.path.expanduser('~/Vision/data/embedding/frcnn_coco_person/checkpoints/checkpoints_20201008_rpn_3.pkl')
        dataset_a = COCOPersonDataset(root, anno, part='train2014', transforms=transforms)
        anno_path = os.path.expanduser('~/data/annotations/modanet2018_instances_revised.pkl')
        root_modanet = os.path.expanduser('~/data/datasets/modanet/Images')
        dataset_b = ModanetDataset(anno=anno_path, part='train', root=root_modanet, transforms=transforms)
        cfg.class_num_1 = 1
        cfg.class_num_2 = 13
        cfg.first_head.roi_head.class_num = cfg.class_num_1
        cfg.second_head.center_net.class_num = cfg.class_num_2
        dataset = MixDataset(dataset_a, dataset_b, shuffle=False, sample=None, add_dataset_label=True)
        #test_dataset = ModanetDataset(anno=anno_path, part='val', root=root, transforms=None) # the default transform in faster rcnn will be used
        if args.evaluate:
            test_dataset1 = COCOPersonDataset(root, anno, part='val2014', transforms=None, dataset_label=cfg.first_head.roi_head.dataset_label)
            test_dataset2 = ModanetDataset(anno=anno_path, part='val', root=root_modanet, transforms=None)
            test_dataset = [test_dataset1, test_dataset2]
        else:
            test_dataset = COCOPersonDataset(root, anno, part='val2014', transforms=None, dataset_label=cfg.first_head.roi_head.dataset_label)
        train_sampler = MixBatchSampler([len(dataset_a), len(dataset_b)], mode='balance', 
                                  batch_size=4, shuffle=True, drop_last=True)
    #collate_fn_rcnn = CollateFnRCNN(min_size=cfg.min_size, max_size=cfg.max_size)

    if 'mix' not in args.dataset:
        data_loader = torch.utils.data.DataLoader(
                dataset, 
                batch_size=batch_size_per_gpu_per_accumulation, 
                shuffle=True, 
                num_workers=4,
                pin_memory=True,
                drop_last=False,
                collate_fn=collate_fn_rcnn)
    else:
        data_loader = torch.utils.data.DataLoader(
            dataset,
            batch_sampler=train_sampler,
            num_workers=4,
            pin_memory=True,
            collate_fn=collate_fn_rcnn
        )


    if args.evaluate:
        test_data_loader0 = torch.utils.data.DataLoader(
            test_dataset[0], 
            batch_size=1, 
            shuffle=False,
            num_workers=0,
            pin_memory=True,
            drop_last=False,
            collate_fn=collate_fn_rcnn
        )
        test_data_loader1 = torch.utils.data.DataLoader(
            test_dataset[1], 
            batch_size=1, 
            shuffle=False,
            num_workers=0,
            pin_memory=True,
            drop_last=False,
            collate_fn=collate_fn_rcnn
        )
        test_data_loader = [test_data_loader0, test_data_loader1]
    else:
        test_data_loader = torch.utils.data.DataLoader(
        test_dataset, 
        batch_size=1, 
        shuffle=False,
        num_workers=0,
        pin_memory=True,
        drop_last=False,
        collate_fn=collate_fn_rcnn
        )

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    model = get_model(cfg )


    #load_checkpoint(model, cfg.path.CHECKPOINT.format(8), device)
    if args.dataset == 'mix':
        if args.evaluate:
            dataset_name = ['coco_person', 'modanet']
        else:
            dataset_name = 'modanet'
    elif args.dataset == 'mix_fashionpedia':
        if args.evaluate:
            dataset_name = ['coco_person', 'fashionpedia']
        else:
            dataset_name = 'fashionpedia'
    elif args.dataset == 'roi_fashionpedia':
        dataset_name = 'fashionpedia'
    else:
        dataset_name = args.dataset

    #test_data_loader = None
    t = my_trainer( cfg, model, device, data_loader, testset=test_data_loader, dataset_name=dataset_name, benchmark=None, val_dataset=None, clip_gradient=clip_gradient, tag=args.tag )
    #loss = t.trainstep()
    #print('loss is {}'.format(loss))
    #t.train_one_epoch()
    #t.eval_result(dataset='coco_person')
    #t.validate_onece()
    #t.load_training(cfg.path.CHECKPOINT.format(8), device)
    #t.validate_onece()
    if not args.evaluate:
        t.train()
    else:
        if args.load_model_path is not None:
            load_checkpoint(model, args.load_model_path, device, to_print=True)
        t.validate_mix()
    #t.train_modanet_human()

    #train_feeder.exit()
    #test_feeder.exit()

    #torch.save({'state_dict':model.state_dict()}, cfg.path.MODEL)

    #print('Model is saved to :', cfg.path.MODEL)
