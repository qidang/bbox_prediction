#import _init
import sys
import os
import argparse
import pickle
import platform
import progressbar
import numpy as np
import datetime
from torchcore.tools import Logger
import torchvision
import math
from torch import nn
import json
import tqdm
import time
from pprint import pprint

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
import datetime
from torchcore.data.transforms import RandomMirror, RandomAbsoluteScale, RandomCrop, Compose

from PIL.ImageDraw import Draw

from rcnn_config import config
#from tools import torch_tools
#from data import data_feeder

from torchcore.data.datasets import ModanetDataset, ModanetHumanDataset, COCOPersonDataset, COCODataset, COCOTorchVisionDataset
from torchcore.data.datasets.fashion_pedia import FashionPediaDataset
#from rcnn_dnn.networks import networks
#from rcnn_dnn.data.collate_fn import collate_fn, CollateFnRCNN
from torchcore.data.collate import CollateFnRCNN, collate_fn_torchvision

import torch
#torch.multiprocessing.set_sharing_strategy('file_system')
import torchvision.transforms as transforms
import torch.optim as optim

from torchcore.dnn import trainer
from torchcore.dnn.networks.faster_rcnn_fpn import FasterRCNNFPN
from torchcore.dnn.networks.roi_net import RoINet
from torchcore.dnn.networks.rpn import MyAnchorGenerator, MyRegionProposalNetwork
from torchcore.dnn.networks.heads import RPNHead
from torchcore.dnn import networks
from torchcore.dnn.networks.tools.data_parallel import DataParallel

def parse_commandline():
    parser = argparse.ArgumentParser(description="Training the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    #parser.add_argument('--patchsize',help='Patch Size', required=True)
    parser.add_argument('-b','--batch_size',help='Batch size per step per gpu', required=True, type=int)
    parser.add_argument('-a','--accumulation_step',help='Accumulate size', required=False, default=1, type=int)
    parser.add_argument('-t','--tag',help='Model tag', required=False)
    parser.add_argument('--resume',help='resume the model', action='store_true', required=False)
    parser.add_argument('--dataset', help='The dataset we are going to use', default='coco_person')
    parser.add_argument('--torchvision_model', help='Do we want to use torchvision model', action='store_true')
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return parser.parse_args()

def get_absolute_box(human_box, box):
    #box[2]+=int(human_box[0])+box[0]
    #box[3]+=int(human_box[1])+box[1]
    box[0]+=int(human_box[0])
    box[1]+=int(human_box[1])
    return box

class my_trainer(trainer):
    def __init__( self, cfg, model, device, trainset, testset=None, dataset_name='modanet', benchmark=None, criterias=None, val_dataset=None, tag=None ):
        self._cfg = cfg
        self._device = device
        self._optimizer = None
        self._model = model

        self._trainset = trainset
        self._testset = testset
        self._benchmark = benchmark
        self._criterias = criterias
        self._dataset_name = dataset_name
        self._epoch = 0
        self._val_dataset = val_dataset
        self._niter = cfg.optimizer.n_iter
        self._tag = tag

        self._set_optimizer()
        self._set_scheduler()
        if cfg.resume:
            path = os.path.join(os.path.dirname(cfg.path.CHECKPOINT), 'last.pth') 
            self.resume_training(path, device)

        #model = DataParallel(model,
        #    device_ids=None, 
        #    chunk_sizes=None)
        model.to(device)
        self._model = model

        self.init_logger()

    def train( self ):
        #if self._testset is not None :
        #    self._validate()
        #    self._validate_torchvision()

        for i in range( self._epoch+1, self._niter+1 ):
            print("Epoch %d/%d" % (i,self._niter))
            self._logger.info('epoch {}\n'.format(i))
            self._train()

            if self._testset is not None and i%1 == 0 :
                if self._cfg.torchvision_model:
                    self._validate_torchvision()
                else:
                    self._validate()
            self._epoch = i
            self.save_training(self._cfg.path.CHECKPOINT.format(self._epoch))
            if hasattr(self, '_scheduler'):
                self._scheduler.step()

    def validate_onece(self):
        self._validate()

    def _validate( self ):
        print('start to validate')
        self._model.eval()

        results = []
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(tqdm.tqdm(self._testset, 'evaluating')):
                inputs = self._set_device( inputs )
                output = self._model( inputs)
                batch_size = len(output['boxes'])
                #for i, im in enumerate(output):
                for i in range(batch_size):
                    if len(output['boxes'][i]) == 0:
                        continue
                    # convert to xywh
                    output['boxes'][i][:,2] -= output['boxes'][i][:,0]
                    output['boxes'][i][:,3] -= output['boxes'][i][:,1]
                    for j in range(len(output['boxes'][i])):
                        results.append({'image_id':int(targets[i]['image_id']), 
                                        'category_id':output['labels'][i][j].cpu().numpy().round(decimals=2).tolist(), 
                                        'bbox':output['boxes'][i][j].cpu().numpy().round(decimals=2).tolist(), 
                                        'score':output['scores'][i][j].cpu().numpy().round(decimals=2).tolist()})
                #output = self._model['net']( inputs, just_embedding=True) # debug
                #bench.update( targets, output )
                #if idx ==30:
                #    break

        out_path = '{}_result.json'.format(self._tag)
        with open(out_path,'w') as f:
            json.dump(results,f)
        self.eval_result(out_path, dataset=self._dataset_name)
    
    def _validate_torchvision( self ):
        print('start to validate')
        self._model.eval()

        results = []
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(tqdm.tqdm(self._testset, 'evaluating')):
                inputs = self._set_device( inputs )
                #if self._cfg.torchvision_model:
                #    output = self._model( inputs['data'])
                #else:
                output = self._model( inputs)
                batch_size = len(output)
                #for i, im in enumerate(output):
                for i in range(batch_size):
                    if len(output[i]['boxes']) == 0:
                        continue
                    # convert to xywh
                    output[i]['boxes'][:,2] -= output[i]['boxes'][:,0]
                    output[i]['boxes'][:,3] -= output[i]['boxes'][:,1]
                    for j in range(len(output[i]['boxes'])):
                        results.append({'image_id':int(targets[i]['image_id']), 
                                        'category_id':output[i]['labels'][j].cpu().numpy().round(decimals=2).tolist(), 
                                        'bbox':output[i]['boxes'][j].cpu().numpy().round(decimals=2).tolist(), 
                                        'score':output[i]['scores'][j].cpu().numpy().round(decimals=2).tolist()})
                #output = self._model['net']( inputs, just_embedding=True) # debug
                #bench.update( targets, output )
                
        out_path = '{}_result.json'.format(self._tag)
        with open(out_path,'w') as f:
            json.dump(results,f)
        self.eval_result(out_path, dataset=self._dataset_name)

    def eval_result(self, dt_json, dataset='coco_person'):
        if dataset not in ['coco','coco_person', 'modanet', 'fashionpedia']:
            raise ValueError('only support coco, coco_person and modanet dataset')
        if dataset == 'coco_person':
            gt_json=os.path.expanduser('~/data/datasets/COCO/annotations/instances_val2014.json')
        elif dataset == 'coco':
            gt_json=os.path.expanduser('~/data/datasets/COCO/annotations/instances_val2017.json')
        elif dataset =='modanet':
            gt_json=os.path.expanduser('~/data/datasets/modanet/Annots/modanet_instances_val.json')
        elif dataset =='fashionpedia':
            gt_json=os.path.expanduser('~/data/datasets/Fashionpedia/annotations/instances_attributes_val2020.json')
        #dt_json='temp_result.json'

        # we need to map the category ids back
        if dataset == 'coco':
            print('revise coco dataset label to gt')
            cat_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 67, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 84, 85, 86, 87, 88, 89, 90]
            with open(dt_json) as f:
                results = json.load(f)
                for result in results:
                    temp_id = result['category_id']
                    result['category_id'] = cat_ids[temp_id-1]
            with open(dt_json,'w') as f:
                json.dump(results,f)
        if dataset == 'fashionpedia':
            print('revise fashionpedia dataset label to gt')
            with open(dt_json) as f:
                results = json.load(f)
                for result in results:
                    temp_id = result['category_id']
                    result['category_id'] = temp_id-1
            with open(dt_json,'w') as f:
                json.dump(results,f)
        annType = 'bbox'
        cocoGt=COCO(gt_json)
        cocoDt=cocoGt.loadRes(dt_json)

        imgIds=sorted(cocoGt.getImgIds())

        # running evaluation
        cocoEval = COCOeval(cocoGt,cocoDt,annType)
        if dataset == 'coco_person':
            cocoEval.params.catIds = [1]
        cocoEval.params.imgIds = imgIds
        cocoEval.evaluate()
        cocoEval.accumulate()
        cocoEval.summarize()

    def _train( self ):
        self._model.train()

        loss_values = []
        average_losses = {}

        self._optimizer.zero_grad()
        back_time = 0
        forward_time = 0
        for idx, (inputs, targets) in enumerate(tqdm.tqdm(self._trainset, desc='Training')):
            #print('inputs:', inputs)
            #print('targets:', targets)

            f_s = time.time()
            inputs = self._set_device( inputs )
            targets = self._set_device( targets )
            #print('input device:', inputs[0].device)

            #if self._cfg.torchvision_model:
            #    loss_dict = self._model(inputs['data'], targets)
            #else:
            loss_dict = self._model(inputs, targets)
            forward_time += time.time() - f_s

            # add the losses for each part
            #loss_sum = sum(loss for loss in loss_dict.values())
            loss_sum=0
            for single_loss in loss_dict:
                loss_sum = loss_sum + loss_dict[single_loss]
                if single_loss in average_losses:
                    average_losses[single_loss]+= loss_dict[single_loss].item()
                else:
                    average_losses[single_loss] = loss_dict[single_loss].item()

            loss_sum_num = loss_sum.item()
            if not math.isfinite(loss_sum_num):
                #print("Loss is {}, stopping training".format(loss_sum))
                self._optimizer.zero_grad()
                print('wrong targets:',targets)
                print("Loss is {}, skip this batch".format(loss_sum))
                print(loss_dict)
                continue
                #sys.exit(1)

            b_s = time.time()
            # Computing gradient and do SGD step
            loss_sum.backward()
            if idx % self._cfg.accumulation_step == 0:
                self._optimizer.step()
                self._optimizer.zero_grad()
            loss_values.append( loss_sum_num )
            back_time += time.time() - b_s

            if idx%1000 == 0:
                self._logger.info('{} '.format(idx+1))
                loss_str = ''
                for loss in average_losses:
                    if idx==0:
                        loss_num = average_losses[loss]
                    else:
                        loss_num = average_losses[loss] / 1000
                    self._logger.info('{} '.format(loss_num))
                    loss_str += (' {} loss:{}, '.format(loss, loss_num))
                print(loss_str[:-2])
                average_losses = {}
                self._logger.info('\n')
                #print(self._model.total_time)
                #self._model.reset_time()
                #print('forward time:', forward_time)
                #forward_time = 0
                #print('back time:', back_time)
                #back_time = 0
            #if idx == 30:
            #    break
            
            #bar.update(idx+1,loss=loss_sum.item())
        #bar.finish()

        print('Average loss : ', np.mean(loss_values))

    def _set_device( self, blobs ):
        if type(blobs) == list:
            for i in range(len(blobs)):
                blobs[i] = self._set_device(blobs[i])
        elif type(blobs) == dict:
            for key, data in blobs.items():
                blobs[key] = self._set_device(data)
        elif torch.is_tensor(blobs):
            blobs = blobs.to(self._device)
        return blobs

    def _set_scheduler(self):
        scheduler = self._cfg.scheduler
        if scheduler.type == 'multi_step':
            self._scheduler = optim.lr_scheduler.MultiStepLR( self._optimizer,
                                                            milestones=scheduler['milestones'],
                                                            gamma=scheduler.get('gamma', 0.1))
        else:
            raise ValueError('unknow scheduler {}'.format(scheduler.type))

    def init_logger(self):
        train_path = self._cfg.path['LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=train_path, console=False, console_formatter=console_formatter)

        print('Loss log path is: {}'.format(train_path))

    def save_training(self, path, to_print=True):
        if isinstance(model, torch.nn.DataParallel):
            state_dict = self._model.module.state_dict()
        else:
            state_dict =self._model.state_dict()
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': state_dict,
            'optimizer_state_dict': self._optimizer.state_dict(),
            'scheduler':self._scheduler.state_dict(),
            'niter':self._niter
        }, path)
        folder = os.path.dirname(path)
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': state_dict,
            'optimizer_state_dict': self._optimizer.state_dict(),
            'scheduler':self._scheduler.state_dict(),
            'niter':self._niter
        }, os.path.join(folder, 'last.pth'))
        if to_print:
            print('The checkpoint has been saved to {}'.format(path))

    def resume_training(self, path, device, to_print=True):
        checkpoint = torch.load(path, map_location=device)
        self._epoch = checkpoint['epoch']
        self._model.load_state_dict(checkpoint['model_state_dict'])
        self._optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        self.move_optimizer_to_device(self._optimizer, device)
        self._niter = self._niter
        if 'scheduler' in checkpoint:
            self._scheduler.load_state_dict(checkpoint['scheduler'])
        if to_print:
            print('Chekpoint has been loaded from {}'.format(path))
    
    def move_optimizer_to_device(self, optimizer, device):
        for state in optimizer.state.values():
            for k, v in state.items():
                if isinstance(v, torch.Tensor):
                    state[k] = v.to(device)

def load_checkpoint(model, path, device, to_print=True):
    #checkpoint = torch.load(path)
    state_dict_ = torch.load(path, map_location=device)['model_state_dict']
    state_dict = {}
    for k in state_dict_:
        if k.startswith('module') and not k.startswith('module_list'):
            state_dict[k[7:]] = state_dict_[k]
        else:
            state_dict[k] = state_dict_[k]
    model.load_state_dict(state_dict, strict=True )
    #self._epoch = checkpoint['epoch']
    #self._model.load_state_dict(checkpoint['model_state_dict'])
    #self._optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    if to_print:
        print('Chekpoint has been loaded from {}'.format(path))

def get_model(cfg, torchvision_model=False):
    if torchvision_model:
        print('use torchvision model')
        backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet50', pretrained=True)
        model = torchvision.models.detection.FasterRCNN(backbone, num_classes=cfg.class_num, min_size=cfg.min_size, max_size=cfg.max_size)
    else:
        backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet50', pretrained=True)
        heads = {}
        anchor_generator = MyAnchorGenerator(sizes=((32,),(64,),(128,),(256,),(512,)), aspect_ratios=(0.5,1.0,2.0))
        #anchor_generator = MyAnchorGenerator(sizes=((32,),(64,),(128,),(256,)), aspect_ratios=(0.5,1.0,2.0))
        num_anchors_per_location = anchor_generator.num_anchors_per_location()
        assert all(num_anchors_per_location[0] == item for item in num_anchors_per_location)
        #print('num anchors per location',num_anchors)
        rpn_head = RPNHead(256, num_anchors=num_anchors_per_location[0])
        rpn = MyRegionProposalNetwork(anchor_generator, rpn_head, cfg.rpn)
        roi_head = RoINet(cfg.roi_head)
        heads['rpn'] = rpn
        heads['bbox'] = roi_head
        #parts = ['heatmap', 'offset', 'width_height']
        model = FasterRCNNFPN(backbone, heads=heads, cfg=cfg, training=True, debug_time=False, feature_names=['0','1','2','3','pool'] )
    return model


if __name__=="__main__" :
    args = parse_commandline()
    params = {}
    params['config'] = 'frcnn_{}'.format(args.dataset)
    params['tag'] = args.tag
    params['path'] = {}
    batch_size_per_gpu_per_accumulation = args.batch_size

    params['path']['project'] = os.path.expanduser('~/Vision/data')
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    #device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], params['path']['project'] )
    cfg.update( args )
    cfg.resume = args.resume
    cfg.out_feature_num = 256
    cfg.accumulation_step = args.accumulation_step
    cfg.nms_thresh = 0.5
    cfg.batch_size = args.batch_size
    cfg.optimizer.lr = cfg.optimizer.lr / args.accumulation_step
    #cfg.min_size = (640, 672, 704, 736, 768, 800)
    cfg.min_size = 1024
    cfg.max_size = 1024
    cfg.torchvision_model = args.torchvision_model

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], args.dataset, model_hash='frcnn' )
    print(cfg)

    t_mirrow = RandomMirror(probability=0.5)
    t_scale = RandomAbsoluteScale(cfg.max_size/2, cfg.max_size*2)
    t_crop = RandomCrop(cfg.max_size, box_inside=True)
    transforms = Compose([t_mirrow, t_scale, t_crop])
    xyxy = True

    if args.dataset == 'coco_person':
        root = os.path.expanduser('~/data/datasets/COCO')
        anno = os.path.expanduser('~/data/annotations/coco2014_instances_person_all.pkl')
        dataset = COCOPersonDataset(root, anno, part='train2014', transforms=transforms)
        test_dataset = COCOPersonDataset(root, anno, part='val2014', transforms=None)
        cfg.class_num = 1
    elif args.dataset == 'modanet':
        anno_path = os.path.expanduser('~/data/annotations/modanet2018_instances_revised.pkl')
        root = os.path.expanduser('~/data/datasets/modanet/Images')
        dataset = ModanetDataset(anno=anno_path, part='train', root=root, transforms=transforms) # the default transform in faster rcnn will be used
        test_dataset = ModanetDataset(anno=anno_path, part='val', root=root, transforms=None) # the default transform in faster rcnn will be used
        cfg.class_num = 13
    elif args.dataset == 'coco':
        anno_path = os.path.expanduser('~/data/annotations/coco2017_instances.pkl')
        root = os.path.expanduser('~/data/datasets/COCO')
        if args.torchvision_model:
            dataset = COCOTorchVisionDataset(anno=anno_path, part='train2017', root=root, transforms=transforms ) # the default transform in faster rcnn will be used
            test_dataset = COCOTorchVisionDataset(anno=anno_path, part='val2017', root=root, transforms=None ) # the default transform in faster rcnn will be used
            cfg.class_num = 81
        else:
            dataset = COCODataset(anno=anno_path, part='train2017', root=root, transforms=transforms, xyxy=xyxy) # the default transform in faster rcnn will be used
            test_dataset = COCODataset(anno=anno_path, part='val2017', root=root, transforms=None, xyxy=xyxy) # the default transform in faster rcnn will be used
            cfg.class_num = 80
    elif args.dataset == 'fashionpedia':
        anno_path_train = os.path.expanduser('~/data/annotations/fashionpedia_instances_train.pkl')
        anno_path_val = os.path.expanduser('~/data/annotations/fashionpedia_instances_val.pkl')
        root = os.path.expanduser('~/data/datasets/Fashionpedia')
        dataset = FashionPediaDataset(anno=anno_path_train, part='train', root=root, transforms=transforms) # the default transform in faster rcnn will be used
        test_dataset = FashionPediaDataset(anno=anno_path_val, part='val', root=root, transforms=None) # the default transform in faster rcnn will be used
        cfg.class_num = 46

    cfg.roi_head.class_num = cfg.class_num
    collate_fn_rcnn_train = CollateFnRCNN(min_size=cfg.min_size, max_size=cfg.max_size, resized=True)
    collate_fn_rcnn_test = CollateFnRCNN(min_size=cfg.min_size, max_size=cfg.max_size)

    if args.torchvision_model:
        collate_fn_rcnn = collate_fn_torchvision

    #collate_fn_rcnn = CollateFnRCNN(min_size=416, max_size=416)

    data_loader = torch.utils.data.DataLoader(
            dataset, 
            batch_size=batch_size_per_gpu_per_accumulation, 
            shuffle=True, 
            num_workers=4,
            pin_memory=True,
            drop_last=False,
            collate_fn=collate_fn_rcnn_train)

    test_data_loader = torch.utils.data.DataLoader(
      test_dataset, 
      batch_size=1, 
      shuffle=False,
      num_workers=0,
      pin_memory=True,
      drop_last=False,
      collate_fn=collate_fn_rcnn_test
    )

    #data_loader_test = torch.utils.data.DataLoader(
    #    val_dataset, batch_size=1, shuffle=False, num_workers=4,
    #    collate_fn=collate_fn_rcnn)

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    model = get_model(cfg, torchvision_model=args.torchvision_model)

    #backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet50', pretrained=True)
    #heads = {}
    #anchor_generator = MyAnchorGenerator(sizes=((32,),(64,),(128,),(256,),(512,)), aspect_ratios=(0.5,1.0,2.0))
    #num_anchors_per_location = anchor_generator.num_anchors_per_location()
    #assert all(num_anchors_per_location[0] == item for item in num_anchors_per_location)
    ##print('num anchors per location',num_anchors)
    #rpn_head = RPNHead(256, num_anchors=num_anchors_per_location[0])
    #rpn = MyRegionProposalNetwork(anchor_generator, rpn_head, cfg.rpn)
    #roi_head = RoINet(cfg)
    #heads['rpn'] = rpn
    #heads['bbox'] = roi_head
    ##parts = ['heatmap', 'offset', 'width_height']
    #model = FasterRCNNFPN(backbone, heads=heads, cfg=cfg, training=True)

    #load_checkpoint(model, cfg.path.CHECKPOINT.format(8), device)

    t = my_trainer( cfg, model, device, data_loader, testset=test_data_loader, dataset_name=args.dataset, benchmark=None, val_dataset=None, tag=args.tag )
    #loss = t.trainstep()
    #print('loss is {}'.format(loss))
    #t.train_one_epoch()
    #t.eval_result(dataset='coco_person')
    #t.validate_onece()
    #t.load_training(cfg.path.CHECKPOINT.format(8), device)
    #t.validate_onece()
    t.train()
    #t.train_modanet_human()

    #train_feeder.exit()
    #test_feeder.exit()

    #torch.save({'state_dict':model.state_dict()}, cfg.path.MODEL)

    #print('Model is saved to :', cfg.path.MODEL)
