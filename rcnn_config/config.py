from easydict import EasyDict as edict
import sys
import os
import platform
import numpy as np
from pprint import pprint
from inspect import isfunction

def mkdir( d ):
    if not os.path.isdir( d ) :
        os.mkdir( d )

class config :
    def __init__( self, cfg_name, projectpath ):
        self._cfg_name = cfg_name
        self._cfg = None
        self._mcfg = None

        self.initialize()
        #self.initialize_dataset( datasetpath )
        self.initialize_data( projectpath )

        #self._init_detector()

    def initialize( self):
        from .configs import configurations
        assert self._cfg_name in configurations, "Unknown config"

        cfg = configurations[ self._cfg_name ].get_cfg()
        assert cfg.NAME == self._cfg_name, "Config name does not match"

        self._cfg = cfg
        for item in cfg:
            setattr(self, item, cfg.__getattribute__(item))
        

        #self._cfg.NAME = cfg.NAME
        #self._cfg.DATA = cfg.DATA
        #self._cfg.DATASET = cfg.DATASET

        #self._cfg.DNN = cfg.DNN[ mode ]
        #self._cfg.DNN.NETWORK = cfg.NETWORK

    #def initialize_dataset( self, datasetpath ):
    #    self._cfg.DATASET.ROOT = datasetpath
    #    self._cfg.DATASET.ANNOTS_TMP = "%s/annotations/%s.pkl" % ( self._cfg.DATASET.ROOT, '%s' )
    #    self._cfg.DATASET.IMAGES_TMP = "%s/datasets/%s" % ( self._cfg.DATASET.ROOT,'%s' )
    #    self._cfg.DATASET.WEIGHTS_TMP = "%s/weights/%s" % ( self._cfg.DATASET.ROOT, '%s' )
    #    self._cfg.DATASET.PRETRAINED = "%s/pretrained/%s.ckpt" % ( self._cfg.DATASET.ROOT, '%s' )
    #    self._cfg.DATASET.WORKSET_SETTINGS = []

    #    if self._cfg.DNN.NETWORK.PRETRAINED is not None :
    #        self._cfg.DATASET.PRETRAINED = self._cfg.DATASET.PRETRAINED % ( self._cfg.DNN.NETWORK.PRETRAINED )

    def initialize_data( self, projectpath ):
        tmp = edict()

        tmp.PROJECT = "bbox"
        tmp.TAG = self._cfg.NAME

        tmp.DIRS = edict()
        tmp.DIRS.ROOT = projectpath
        tmp.DIRS.BASE = os.path.join( tmp.DIRS.ROOT, tmp.PROJECT )

        mkdir( tmp.DIRS.ROOT )
        mkdir( tmp.DIRS.BASE )

        tmp.DIRS.BASE = os.path.join( tmp.DIRS.BASE, tmp.TAG )
        mkdir( tmp.DIRS.BASE )
        tmp.DIRS.LOGS = os.path.join( tmp.DIRS.BASE, "logs" )
        mkdir( tmp.DIRS.LOGS )
        #tmp.DIRS.DETS = os.path.join( tmp.DIRS.BASE, "dets" )
        #mkdir( tmp.DIRS.DETS )
        tmp.DIRS.CHECKPOINTS = os.path.join( tmp.DIRS.BASE, "checkpoints" )
        mkdir( tmp.DIRS.CHECKPOINTS )
        #tmp.DIRS.BENCHMARK = os.path.join( tmp.DIRS.BASE, "benchmark" )
        #mkdir( tmp.DIRS.BENCHMARK )
        #tmp.DIRS.HDF5 = os.path.join( tmp.DIRS.BASE, "hdf5" )
        #mkdir( tmp.DIRS.HDF5 )

        self._cfg.DATA = tmp


    def build_path( self, tag, data_hash, model_hash=None ):
        assert( self._cfg.DATA is not None )
        data = self._cfg.DATA

        if model_hash is None :
            model_hash = data_hash

        path = edict()
        #path.MODEL = '%s/model_%s_%s_%s.pkl' % ( data.DIRS.MODELS, dnn.NETWORK.FEAT_NAME, model_hash, tag )
        #path.HDF5 = '%s/%s.hdf5' % ( data.DIRS.HDF5, '%s' )
        path.LOG = '%s/log_%s_%s.csv' % ( data.DIRS.LOGS, model_hash, tag )
        #path.RES  = '%s/results_model_%s_dset_%s_%s.pkl' % ( data.DIRS.DETS, model_hash, data_hash, tag )
        #path.BENCHMARK = '%s/bench_model_%s_dset_%s_%s_%s.pkl' % ( data.DIRS.BENCHMARK, model_hash, data_hash, tag, '%s' )
        #path.BENCHMARK_LOG = '%s/bench_log_%s_%s_%s.csv' % ( data.DIRS.BENCHMARK, dnn.NETWORK.FEAT_NAME, model_hash, tag )
        path.CHECKPOINT = '{}/checkpoints_{}_{{}}.pkl'.format(data.DIRS.CHECKPOINTS, tag)

        self._cfg.PATH = path

    def update( self, args ):
        if hasattr(args, 'roi_w'):
            if args.roi_w is not None:
                self.second_head.roi_pool_w = args.roi_w
                self.targets_converter.roi_pool_w = self.second_head.roi_pool_w
                self.inputs_converter.roi_pool_w = self.second_head.roi_pool_w
                if hasattr(self.second_head, 'center_net'):
                    self.second_head.center_net.roi_pool_w = self.second_head.roi_pool_w
                if hasattr(self, 'third_head'):
                    self.third_head.roi_pool_w = args.roi_w
                if hasattr(self, 'roi_pooler'):
                    self.roi_pooler.roi_pool_w = self.second_head.roi_pool_w
                if hasattr(self, 'roi_pooler1'):
                    self.roi_pooler1.roi_pool_w = self.second_head.roi_pool_w
                if hasattr(self, 'roi_pooler2'):
                    self.roi_pooler2.roi_pool_w = self.second_head.roi_pool_w

        if hasattr(args, 'roi_h'):
            if args.roi_h is not None:
                self.second_head.roi_pool_h = args.roi_h
                self.targets_converter.roi_pool_h = self.second_head.roi_pool_h
                self.inputs_converter.roi_pool_h = self.second_head.roi_pool_h
                if hasattr(self.second_head, 'center_net'):
                    self.second_head.center_net.roi_pool_h = self.second_head.roi_pool_h
                if hasattr(self, 'third_head'):
                    self.third_head.roi_pool_h = args.roi_h
                if hasattr(self, 'roi_pooler'):
                    self.roi_pooler.roi_pool_h = self.second_head.roi_pool_h
                if hasattr(self, 'roi_pooler1'):
                    self.roi_pooler1.roi_pool_h = self.second_head.roi_pool_h
                if hasattr(self, 'roi_pooler2'):
                    self.roi_pooler2.roi_pool_h = self.second_head.roi_pool_h
        
        if hasattr(args, 'second_loss_weight'):
            self.second_loss_weight = args.second_loss_weight
        else:
            self.second_loss_weight = None

        if hasattr(args, 'third_loss_weight'):
            self.third_loss_weight = args.third_loss_weight
        else:
            self.third_loss_weight = None
        
        if hasattr(args, 'freeze_bn'):
            self.freeze_bn = args.freeze_bn

        self.resume = args.resume
        self.accumulation_step = args.accumulation_step
        self.batch_size = args.batch_size
        if hasattr(self.optimizer, 'lr'):
            self.optimizer.lr = self.optimizer.lr / args.accumulation_step
    
    def update_lr(self, batch_size):
        self.optimizer.lr = self.optimizer.element_lr * batch_size * self.accumulation_step
        self.optimizer.total_step = self.optimizer.element_step // batch_size
        #self.scheduler.milestones = [int(2/3*self.optimizer.total_step), int(8/9*self.optimizer.total_step)]
        self.scheduler.milestones = [int(part*self.optimizer.total_step) for part in self.scheduler.milestones_split]

    def __str__(self):
        for attri in dir(self):
            if isfunction(self.__getattribute__(attri)) or callable(self.__getattribute__(attri)) or attri.startswith('__'):
                continue
            else:
                print("{}: {}".format(attri, self.__getattribute__(attri)))
        return ''


    #@property
    #def dnn( self ):
    #    return self._cfg.DNN

    @property
    def data( self ):
        return self._cfg.DATA

    #@property
    #def dataset( self ):
    #    return self._cfg.DATASET

    @property
    def path( self ):
        return self._cfg.PATH

