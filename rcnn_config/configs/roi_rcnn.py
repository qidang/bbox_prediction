from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg():
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'roi_rcnn'

    roi_pooler = edict()
    roi_pooler.dataset_label = None
    roi_pooler.roi_pool_w = 56
    roi_pooler.roi_pool_h = 56
    roi_pooler.min_area = 256 # minimum human box area
    roi_pooler.train_box_iou_thre = 0.7
    cfg.roi_pooler = roi_pooler

    targets_converter = edict()
    targets_converter.roi_pool_w = roi_pooler.roi_pool_w
    targets_converter.roi_pool_h = roi_pooler.roi_pool_h
    cfg.targets_converter = targets_converter

    inputs_converter = edict()
    inputs_converter.roi_pool_w = roi_pooler.roi_pool_w
    inputs_converter.roi_pool_h = roi_pooler.roi_pool_h
    cfg.inputs_converter = inputs_converter


    roi_detection_head = edict()
    roi_detection_head.roi_pool_w = roi_pooler.roi_pool_w
    roi_detection_head.roi_pool_h = roi_pooler.roi_pool_h
    roi_detection_head.just_rpn = False

    rpn = edict()
    rpn.nms_thresh = 0.7
    rpn.min_box_size = 1e-3
    rpn.min_box_size = 3
    rpn.pre_nms_top_n_train = 2000
    rpn.post_nms_top_n_train = 2000
    rpn.pre_nms_top_n_test = 1000
    rpn.post_nms_top_n_test = 1000
    rpn.dataset_label = None
    roi_detection_head.rpn = rpn

    #roi_pool = edict()
    #roi_pool.pool_w = 7
    #roi_pool.pool_h = 7
    #cfg.roi_pool = roi_pool

    roi_head = edict()
    roi_head.score_thre = 0.001
    roi_head.iou_low_thre = 0.5
    roi_head.iou_high_thre = 0.5
    roi_head.pos_sample_num = 128
    roi_head.neg_sample_num = 384
    roi_head.detection_per_image = 100
    roi_head.dataset_label = None
    roi_head.pool_w = 7
    roi_head.pool_h = 7
    roi_head.nms_thresh = 0.5
    roi_head.out_feature_num = 256
    roi_detection_head.roi_head = roi_head

    roi_detection_head.dataset_label = None
    cfg.roi_detection_head = roi_detection_head

    optimizer = edict()
    #optimizer.type = 'Adam'
    #optimizer.lr = 0.0001
    optimizer.type = 'SGD'
    optimizer.lr = 0.0025
    #optimizer.lr = 0.0025
    #optimizer.n_iter = 13
    optimizer.n_iter = 26
    optimizer.weight_decay=1e-4
    cfg.optimizer = optimizer

    scheduler = edict()
    scheduler.type = 'multi_step'
    #scheduler.milestones = [8, 11]
    scheduler.milestones = [16, 22]
    scheduler.gamma = 0.1
    cfg.scheduler = scheduler

    #cfg.DNN['train'].PATCH_SIZE = 64
    #cfg.DNN['test'].PATCH_SIZE = 64

    #cfg.DNN['train'].NITER = 15
    ##old one
    ##cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.01,'decay_step':5,'decay_rate':0.1}
    ##cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.001,'decay_step':12,'decay_rate':0.1}
    #cfg.DNN['train'].OPTIMIZER = {'type':'Adam','lr':0.0001, 'milestones':[10,13], 'gamma':0.1}

    #cfg.NETWORK.FEATURE_DIM = 1024

    ##cfg.DNN.PROPOSAL.TRAIN.SELECTION_BATCH_SIZE = {'s1':128,'s2':128}
    ##cfg.DNN.FRCNN.TRAIN.SELECTION_BATCH_SIZE = {'s1':256,'s2':256,'s2_hinge':2000}

    #cfg.NETWORK.INTERFACE = 'patch'
    #cfg.NETWORK.FEAT_NAME = 'vgg16'

    return cfg
