from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg():
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'retinanet_coco'
    #cfg.DATASET.WORKSET_SETTING = []

    retinahead = edict()
    retinahead.nms_thresh = 0.5
    retinahead.score_thresh = 0.05
    retinahead.min_box_size = 1e-3
    retinahead.min_box_size = 3
    retinahead.pre_nms_top_n_train = 1000
    retinahead.post_nms_top_n_train = 300
    retinahead.pre_nms_top_n_test = 1000
    retinahead.post_nms_top_n_test = 300
    cfg.retinahead = retinahead

    #optimizer = edict()
    ##optimizer.type = 'Adam'
    ##optimizer.lr = 0.0001
    #optimizer.type = 'SGD'
    #optimizer.lr = 0.0025
    #optimizer.n_iter = 26
    #optimizer.weight_decay=1e-4
    #cfg.optimizer = optimizer

    #scheduler = edict()
    #scheduler.type = 'multi_step'
    #scheduler.milestones = [16, 22]
    #scheduler.gamma = 0.1
    #cfg.scheduler = scheduler

    optimizer = edict()
    #optimizer.type = 'Adam'
    #optimizer.lr = 0.0001
    optimizer.type = 'SGD'
    #optimizer.lr = 0.0025
    # lr for batch size one for linear learning rate scaling
    # the real should be lr = element_lr * batch_size
    optimizer.element_lr = 0.00125/2
    #optimizer.lr = optimizer.element_lr * batch_size
    # element_step is the step for batch size one
    # the totol step should be element_step/batch_size
    optimizer.element_step = 1440000
    #optimizer.total_step = optimizer.element_step // batch_size
    #optimizer.n_iter = 13
    optimizer.weight_decay=1e-4
    cfg.optimizer = optimizer

    scheduler = edict()
    scheduler.type = 'multi_step'
    # the first mile stone is 2/3*total_step
    # the second mile stone is 8/9*total_step
    #scheduler.milestones = [8, 11]
    #scheduler.milestones = [int(2/3*optimizer.total_step), int(8/9*optimizer.total_step)]
    scheduler.milestones_split = [2/3, 8/9]
    scheduler.gamma = 0.1
    cfg.scheduler = scheduler
    #cfg.DNN['train'].PATCH_SIZE = 64
    #cfg.DNN['test'].PATCH_SIZE = 64

    #cfg.DNN['train'].NITER = 15
    ##old one
    ##cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.01,'decay_step':5,'decay_rate':0.1}
    ##cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.001,'decay_step':12,'decay_rate':0.1}
    #cfg.DNN['train'].OPTIMIZER = {'type':'Adam','lr':0.0001, 'milestones':[10,13], 'gamma':0.1}

    #cfg.NETWORK.FEATURE_DIM = 1024

    ##cfg.DNN.PROPOSAL.TRAIN.SELECTION_BATCH_SIZE = {'s1':128,'s2':128}
    ##cfg.DNN.FRCNN.TRAIN.SELECTION_BATCH_SIZE = {'s1':256,'s2':256,'s2_hinge':2000}

    #cfg.NETWORK.INTERFACE = 'patch'
    #cfg.NETWORK.FEAT_NAME = 'vgg16'

    return cfg
