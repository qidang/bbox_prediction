from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg():
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'fovea'

    fovea_head = edict()
    fovea_head.in_feature_num = 256
    fovea_head.r_layer = [32,64,128,256,512]
    fovea_head.eta = 2
    fovea_head.sigma = 0.4
    cfg.fovea_head = fovea_head


    optimizer = edict()
    #optimizer.type = 'Adam'
    #optimizer.lr = 0.0001
    optimizer.type = 'SGD'
    optimizer.lr = 0.01
    #optimizer.lr = 0.0025
    optimizer.n_iter = 15
    optimizer.weight_decay=1e-4
    cfg.optimizer = optimizer

    scheduler = edict()
    scheduler.type = 'multi_step'
    scheduler.milestones = [9, 13]
    scheduler.gamma = 0.1
    cfg.scheduler = scheduler


    return cfg
