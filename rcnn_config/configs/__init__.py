from . import base as baseline
from . import bbox
from . import bbox_attention
from . import rcnn_bbox
from . import fovea
from . import rcnn_bbox_garment
from . import frcnn_modanet
from . import frcnn_fashionpedia
from . import roi_rcnn
from . import mask_rcnn_fashionpedia
from . import frcnn_roi_fashionpedia
from . import retinanet_fashionpedia
from . import retinanet_coco
from . import retinanet_roi_fashionpedia


configurations = {}

configurations['baseline'] = baseline
configurations['bbox'] = bbox
configurations['rcnn_bbox'] = rcnn_bbox
configurations['rcnn_bbox_garment'] = rcnn_bbox_garment
configurations['bbox_attention'] = bbox_attention
configurations['fovea'] = fovea
configurations['frcnn_modanet'] = frcnn_modanet
configurations['frcnn_fashionpedia'] = frcnn_fashionpedia
configurations['mask_rcnn_fashionpedia'] = mask_rcnn_fashionpedia
configurations['roi_rcnn'] = roi_rcnn
configurations['frcnn_roi_fashionpedia'] = frcnn_roi_fashionpedia
configurations['retinanet_fashionpedia'] = retinanet_fashionpedia
configurations['retinanet_roi_fashionpedia'] = retinanet_roi_fashionpedia
configurations['retinanet_coco'] = retinanet_coco
