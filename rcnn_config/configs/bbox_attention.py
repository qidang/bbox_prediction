from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg():
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'bbox_attention'

    roi_pool_h = 7
    roi_pool_w = 7
    backbone_feature_num = 256
    feature_num = 1024

    head = edict()
    #head.pool_h= roi_pool_h
    #head.pool_w= roi_pool_w
    head.feature_num = backbone_feature_num
    cfg.head = head

    attention_head = edict()
    attention_head.target_dim = roi_pool_w*roi_pool_h*backbone_feature_num
    attention_head.out_feature_num = feature_num
    attention_head.feature_dim = backbone_feature_num
    cfg.attention_head = attention_head

    roi_pooler = edict()
    roi_pooler.roi_pool_w = roi_pool_w
    roi_pooler.roi_pool_h =  roi_pool_h
    roi_pooler.stride = 4
    roi_pooler.output_size = (roi_pooler.roi_pool_h, roi_pooler.roi_pool_w)
    cfg.roi_pooler = roi_pooler


    optimizer = edict()
    #optimizer.type = 'Adam'
    #optimizer.lr = 0.0001
    optimizer.type = 'SGD'
    optimizer.lr = 0.001
    #optimizer.lr = 0.0025
    optimizer.n_iter = 15
    optimizer.weight_decay=1e-4
    cfg.optimizer = optimizer

    scheduler = edict()
    scheduler.type = 'multi_step'
    scheduler.milestones = [10, 13]
    scheduler.gamma = 0.1
    cfg.scheduler = scheduler


    return cfg
