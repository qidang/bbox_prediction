from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg():
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'bbox'

    head = edict()
    head.pool_h=7
    head.pool_w=7
    head.out_feature_num = 256
    cfg.head = head

    roi_pooler = edict()
    roi_pooler.roi_pool_w = 7
    roi_pooler.roi_pool_h = 7
    roi_pooler.stride = 4
    roi_pooler.output_size = (roi_pooler.roi_pool_h, roi_pooler.roi_pool_w)
    cfg.roi_pooler = roi_pooler


    optimizer = edict()
    #optimizer.type = 'Adam'
    #optimizer.lr = 0.0001
    optimizer.type = 'SGD'
    optimizer.lr = 0.01
    #optimizer.lr = 0.0025
    optimizer.n_iter = 15
    optimizer.weight_decay=1e-4
    cfg.optimizer = optimizer

    scheduler = edict()
    scheduler.type = 'multi_step'
    scheduler.milestones = [10, 13]
    scheduler.gamma = 0.1
    cfg.scheduler = scheduler


    return cfg
