from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg(batch_size=4):
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'frcnn_roi_fashionpedia'
    #cfg.DATASET.WORKSET_SETTING = []

    cfg.roi_w = 256
    cfg.roi_h = 256

    target_converter = edict()
    target_converter.roi_pool_h = cfg.roi_h
    target_converter.roi_pool_w = cfg.roi_w
    target_converter.stride = 4
    target_converter.boxes_key = 'boxes'
    target_converter.keep_key = ['labels']
    target_converter.mask_key = None
    target_converter.allow_box_outside = True
    cfg.target_converter = target_converter

    rpn = edict()
    rpn.nms_thresh = 0.7
    rpn.min_box_size = 1e-3
    rpn.min_box_size = 3
    rpn.pre_nms_top_n_train = 2000
    rpn.post_nms_top_n_train = 2000
    rpn.pre_nms_top_n_test = 1000
    rpn.post_nms_top_n_test = 1000
    cfg.rpn = rpn

    roi_pool = edict()
    roi_pool.pool_w = 7
    roi_pool.pool_h = 7
    cfg.roi_pool = roi_pool

    roi_head = edict()
    roi_head.score_thre = 0.001
    roi_head.iou_low_thre = 0.5
    roi_head.iou_high_thre = 0.5
    roi_head.pos_sample_num = 128
    roi_head.neg_sample_num = 384
    roi_head.detection_per_image = 100
    roi_head.pool_w = 7
    roi_head.pool_h = 7
    roi_head.nms_thresh = 0.5
    roi_head.out_feature_num = 256
    roi_head.box_weight = [10.0, 10.0, 5.0, 5.0]

    # For mask head
    roi_head.mask_head = False
    roi_head.mask_pool_w = 14
    roi_head.mask_pool_h = 14
    cfg.roi_head = roi_head

    optimizer = edict()
    #optimizer.type = 'Adam'
    #optimizer.lr = 0.0001
    optimizer.type = 'SGD'
    #optimizer.lr = 0.0025
    # lr for batch size one for linear learning rate scaling
    # the real should be lr = element_lr * batch_size
    optimizer.element_lr = 0.00125
    #optimizer.lr = optimizer.element_lr * batch_size
    # element_step is the step for batch size one
    # the totol step should be element_step/batch_size
    optimizer.element_step = 1440000
    #optimizer.total_step = optimizer.element_step // batch_size
    #optimizer.n_iter = 13
    optimizer.weight_decay=1e-4
    cfg.optimizer = optimizer

    scheduler = edict()
    scheduler.type = 'multi_step'
    # the first mile stone is 2/3*total_step
    # the second mile stone is 8/9*total_step
    #scheduler.milestones = [8, 11]
    #scheduler.milestones = [int(2/3*optimizer.total_step), int(8/9*optimizer.total_step)]
    scheduler.milestones_split = [2/3, 8/9]
    scheduler.gamma = 0.1
    cfg.scheduler = scheduler

    #cfg.DNN['train'].PATCH_SIZE = 64
    #cfg.DNN['test'].PATCH_SIZE = 64

    #cfg.DNN['train'].NITER = 15
    ##old one
    ##cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.01,'decay_step':5,'decay_rate':0.1}
    ##cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.001,'decay_step':12,'decay_rate':0.1}
    #cfg.DNN['train'].OPTIMIZER = {'type':'Adam','lr':0.0001, 'milestones':[10,13], 'gamma':0.1}

    #cfg.NETWORK.FEATURE_DIM = 1024

    ##cfg.DNN.PROPOSAL.TRAIN.SELECTION_BATCH_SIZE = {'s1':128,'s2':128}
    ##cfg.DNN.FRCNN.TRAIN.SELECTION_BATCH_SIZE = {'s1':256,'s2':256,'s2_hinge':2000}

    #cfg.NETWORK.INTERFACE = 'patch'
    #cfg.NETWORK.FEAT_NAME = 'vgg16'

    return cfg
