import torch
import math
from torch import nn 
from torchcore.dnn.networks.tools import AnchorBoxesCoder

class BBoxPredNet(nn.Module):
    def __init__(self, backbone,  head, roi_pooler, targets_converter=None, neck=None):
        super().__init__()
        self.backbone = backbone
        self.neck = neck
        self.roi_pooler = roi_pooler
        if targets_converter is None:
            self.targets_converter = AnchorBoxesCoder()
        else:
            self.targets_converter = targets_converter

        self.head = head
        self.loss = nn.SmoothL1Loss(reduction='sum', beta= 1.0 / 9)

    def forward(self, inputs, targets=None):
        features = self.backbone(inputs['data'])
        if self.neck is not None:
            self.neck(features)

        #features = {'0': features['0']}

        boxes = inputs['input_box']
        boxes = self.add_batch_ind(boxes)
        roi_features = self.roi_pooler(features['0'], boxes)

        pred = self.head(roi_features )
        
        if self.training:
            if self.targets_converter is not None:
                delta_targets = self.targets_converter.encode_once(inputs['input_box'], targets['target_box'])
            batch_size = len(pred)
            loss = self.loss(pred, delta_targets) / batch_size
            return {'loss': loss}
        else:
            result = {}
            input_boxes = inputs['input_box']
            target_box = self.targets_converter.decode_once(pred, input_boxes)
            result['target_box'] = target_box
            return result

    def add_batch_ind(self, boxes):
        batch_size = len(boxes)
        batch_ind = torch.arange(batch_size, dtype=boxes.dtype, device=boxes.device)[:,None]
        boxes = torch.cat((batch_ind, boxes), dim=1)
        return boxes

class TargetsCoder():
    def __init__(self, box_code_clip=math.log(1000. / 16)) -> None:
        self.box_code_clip = box_code_clip

    def encode(input_box, target_box):
        pass

class BBoxHead(nn.Module):
    def __init__(self, cfg):
        super().__init__()
        self.cfg = cfg
        feature_num = 1024
        pool_h = cfg.pool_h
        pool_w = cfg.pool_w
        self.feature_head = nn.Sequential( 
            nn.Linear(cfg.out_feature_num*pool_h*pool_w, feature_num),
            nn.ReLU(inplace=True),
            nn.Linear(feature_num, feature_num),
            nn.ReLU(inplace=True)
        )
        self.bbox_head = nn.Linear(feature_num, 4)

    def forward(self, rois ):
        rois = rois.flatten(start_dim=1)
        features = self.feature_head(rois)
        bbox_pre = self.bbox_head(features)

        return bbox_pre