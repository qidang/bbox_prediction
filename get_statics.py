import numpy as np
import os
import pickle

def map_to_id(annos):
    out = {}
    for anno in annos:
        out[anno['id']]=anno
    return out
def cal_empty(anno):
    empty_input = 0
    empty_target = 0
    total = 0
    for k,v in anno.items():
        #if 'input_box' not in v:
        #    empty_input += 1
        #if 'target_box' not in v:
        #    empty_target += 1

        if len(v['input_box']) < 4:
            empty_input += 1
        if len(v['target_box']) < 4:
            empty_target += 1
        total += 1
    print('{} samples has no input box'.format(empty_input/total))
    print('{} samples has no target box'.format(empty_target/total))

def cal_missing(annoA, annoB):
    B_missing = 0
    A_missing = 0
    for k in annoA.keys():
        if k not in annoB.keys():
            B_missing += 1

    for k in annoB.keys():
        if k not in annoA.keys():
            A_missing += 1
    print('A missing {}'.format(A_missing))
    print('B missing {}'.format(B_missing))
            
def AIoU(A, B):
    x1 = max(A[0], B[0])
    y1 = max(A[1], B[1])
    x2 = min(A[2], B[2])
    y2 = min(A[3], B[3])
    if x1 >= x2 or y1 >= y2:
        return 0
    uni_box = (x2-x1) * (y2-y1)
    A_area = (A[2]-A[0]) * (A[3]-A[1])
    return uni_box/A_area

def miss_garments(human_anno, garment_anno, thresh=0.5):
    garments_num = 0
    missing_num_input = 0
    missing_num_target = 0
    for k,v in human_anno.items():
        input_box = v['input_box']
        target_box = v['target_box']
        objs = garment_anno[k]['objects']
        for obj in objs:
            bbox = np.copy(obj['bbox'])
            bbox[2] += bbox[0]
            bbox[3] += bbox[1]
            uni_input = AIoU(bbox, input_box)
            uni_target = AIoU(bbox, target_box)
            if uni_input <= thresh:
                missing_num_input += 1
            if uni_target <= thresh:
                missing_num_target += 1
            garments_num += 1
    print('{} of garments are missing in the input box by thresh {}'.format(missing_num_input/garments_num, thresh))
    print('{} of garments are missing in the target box by thresh {}'.format(missing_num_target/garments_num, thresh))

if __name__=='__main__':
    anno_path = os.path.expanduser('~/data/annotations/modanet2018_instances_revised.pkl')
    im_root = os.path.expanduser('~/data/datasets/modanet/Images')
    boxes_path = 'modanet_boxes.pkl'

    with open(boxes_path, 'rb') as f:
        modanet_boxes = pickle.load(f)['train']
    
    #cal_empty(modanet_boxes)


    with open(anno_path, 'rb') as f:
        modanet_anno = pickle.load(f)
    
    
    modanet_anno = modanet_anno['train']+modanet_anno['val']
    print('modanet has {} images'.format(len(modanet_anno)))
    modanet_anno = map_to_id(modanet_anno)

    miss_garments(modanet_boxes, modanet_anno, thresh=0.95)
    #print(modanet_anno.keys())
    #cal_missing(modanet_anno, modanet_boxes)
    #exit()